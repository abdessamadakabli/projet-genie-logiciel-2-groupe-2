load ('../Page.rb')
require "../cases/CaseReponse.rb"

##
#   Cette classe permet de gérer le pavet numérique qui permet à l'utilisateur de saisir une réponse
class PaveNumeriqueReponse < Page

    ##
    #   new étant privée, on utilise cette méthode pour créer un pavé numérique
    def PaveNumeriqueReponse.creer
        new()
    end

    ##
    #   Permet d'initiliser le pavé numérique
    def initialiser()
        @caseReponse_gtk = nil
        @caseReponse_ruby = nil
        @isPerfection = nil
        initialiserSignaux()
    end

    ##
    #   Permet de lier une partie au pavé numérique
    #   @param partieJeu La partie en cours du jeu 
    #   @param isPerfection permet savoir si on est en mode perfection
    def lierPartie(partieJeu, isPerfection)
        @partieJeu = partieJeu
        @isPerfection = isPerfection
    end

    ##
    #   Permet d'afficher le pavé numérique réponse
    #   param c La case du jeu qui sera modifiée par la saisie du joueur
    #   param c2 La partie visible de la case du jeu pour notifier aux joueurs le changement de réponse
    #   param grilleJeu La grille du jeu
    def afficher(c, c2, grilleJeu)
        @caseReponse_ruby = c
        @caseReponse_gtk = c2
        @grilleJeu = grilleJeu
        @fenetre.show()
    end

    ##
    #   Permet de cacher le pavé numérique
    def cacher()
        @fenetre.hide()
    end

    ##
    #	Permet d'initialiser les signaux connectés au pavé numérique (gère les cliques)
    def initialiserSignaux()
        @fenetre.signal_connect('delete_event') {
            @caseReponse_gtk.set_name("white_case_rep")
            cacher()
        }
        @paveEffacer.signal_connect('clicked'){
            @caseReponse_ruby.saisirReponse(-1)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label("")
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"
            cacher()
        }
        @pave1.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":1"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(1)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave2.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":2"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(2)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave3.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":3"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(3)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave4.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":4"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(4)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave5.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":5"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(5)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave6.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":6"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(6)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave7.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":7"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(7)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave8.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":8"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(8)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
        @pave9.signal_connect('clicked'){
            s = String.new()
            s += "saisie_reponse:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":9"
            @grilleJeu.historique.ajout_Action(s)
            @caseReponse_ruby.saisirReponse(9)
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_gtk.set_label(@caseReponse_ruby.reponse.to_s)
            @caseReponse_gtk.set_name("white_case_rep")
            @caseReponse_ruby.couleur = "white_case_rep"

            if (@caseReponse_ruby.verifierReponse() == false && @isPerfection == true)
                @caseReponse_gtk.set_name("red_case")
                @partieJeu.perdu
            end

            cacher()
        }
    end

end
