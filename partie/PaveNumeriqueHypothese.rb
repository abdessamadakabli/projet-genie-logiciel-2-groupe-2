load ('../Page.rb')
require "../cases/CaseReponse.rb"

##
#   Cette classe permet de gérer le pavet numérique qui permet à l'utilisateur de saisir ses hypothèses
class PaveNumeriqueHypothese < Page
    ##
    #   new étant privée, on utilise cette méthode pour créer un pavé numérique
    def PaveNumeriqueHypothese.creer
        new()
    end

    ##
    #   Permet d'initialiser le pavé numérique
    def initialiser()
        @caseReponse_ruby = nil
        @caseReponse_gtk = nil
        initialiserSignaux()
    end

    ##
    #   Permet d'afficher le pavé numérique d'hypothèses
    #   param c La case du jeu qui sera modifiée par la saisie du joueur
    #   param c2 La partie visible de la case du jeu pour notifier aux joueurs les changements d'hypotèses
    #   param grilleJeu La grille du jeu
    def afficher(c, c2, grilleJeu)
        @caseReponse_ruby = c
        @caseReponse_gtk = c2
        @grilleJeu = grilleJeu
        @caseReponse_gtk.set_name("orange_case")
        if (@caseReponse_ruby.existeHypothese?(1))
            @pave1.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(2))
            @pave2.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(3))
            @pave3.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(4))
            @pave4.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(5))
            @pave5.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(6))
            @pave6.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(7))
            @pave7.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(8))
            @pave8.set_name("orange_pave")
        end
        if (@caseReponse_ruby.existeHypothese?(9))
            @pave9.set_name("orange_pave")
        end
        @fenetre.show()
    end

    ##
    #   Permet de cacher le pavé numérique
    def cacher()
        @fenetre.hide()
    end

    ##
    #	Permet d'initialiser les signaux connectés au pavé numérique (gère les clics souris)
    def initialiserSignaux()
        @fenetre.signal_connect('delete_event') {
            @caseReponse_gtk.set_name("white_case_hyp")
            cacher()
        }
        @paveEffacer.signal_connect('clicked'){
            @caseReponse_ruby.effacerHypotheses()
            @caseReponse_ruby.effacerReponse()
            @caseReponse_gtk.set_label("")
            @caseReponse_gtk.set_name("white_case_hyp")
            @caseReponse_ruby.couleur = "white_case_hyp"
            cacher()
        }
        @pave1.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(1))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":1"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(1)
                @pave1.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(1)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":1"
                @grilleJeu.historique.ajout_Action(s)
                @pave1.set_name("orange_pave")
            end
        }
        @pave2.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(2))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":2"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(2)
                @pave2.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(2)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":2"
                @grilleJeu.historique.ajout_Action(s)
                @pave2.set_name("orange_pave")
            end
        }
        @pave3.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(3))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":3"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(3)
                @pave3.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(3)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":3"
                @grilleJeu.historique.ajout_Action(s)
                @pave3.set_name("orange_pave")
            end
        }
        @pave4.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(4))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":4"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(4)
                @pave4.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(4)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":4"
                @grilleJeu.historique.ajout_Action(s)
                @pave4.set_name("orange_pave")
            end
        }
        @pave5.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(5))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":5"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(5)
                @pave5.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(5)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":5"
                @grilleJeu.historique.ajout_Action(s)
                @pave5.set_name("orange_pave")
            end
        }
        @pave6.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(6))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":6"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(6)
                @pave6.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(6)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":6"
                @grilleJeu.historique.ajout_Action(s)
                @pave6.set_name("orange_pave")
            end
        }
        @pave7.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(7))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":7"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(7)
                @pave7.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(7)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":7"
                @grilleJeu.historique.ajout_Action(s)
                @pave7.set_name("orange_pave")
            end
        }
        @pave8.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(8))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":8"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(8)
                @pave8.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(8)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":8"
                @grilleJeu.historique.ajout_Action(s)
                @pave8.set_name("orange_pave")
            end
        }
        @pave9.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            if (@caseReponse_ruby.existeHypothese?(9))
                s = String.new()
                s += "effacer_hypotheses:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":9"
                @grilleJeu.historique.ajout_Action(s)
                @caseReponse_ruby.effacerUneHypothese(9)
                @pave9.set_name("white_case")
            else
                @caseReponse_ruby.saisirHypotheses(9)
                s = String.new()
                s += "saisie_hypothese:"+@caseReponse_ruby.x.to_s+":"+@caseReponse_ruby.y.to_s+":9"
                @grilleJeu.historique.ajout_Action(s)
                @pave9.set_name("orange_pave")
            end
        }
        @paveValider.signal_connect('clicked'){
            @caseReponse_ruby.effacerReponse()
            @caseReponse_gtk.set_label(self.to_s)
            @caseReponse_gtk.set_name("white_case_hyp")
            @caseReponse_ruby.couleur = "white_case_hyp"
            @pave1.set_name("white_case")
            @pave2.set_name("white_case")
            @pave3.set_name("white_case")
            @pave4.set_name("white_case")
            @pave5.set_name("white_case")
            @pave6.set_name("white_case")
            @pave7.set_name("white_case")
            @pave8.set_name("white_case")
            @pave9.set_name("white_case")
            cacher()
        }
    end

    ##
    #   Permet de récupérer la chaine de caractère crée par le pavé numérique
    def to_s()
        chaine = String.new()
        if (@caseReponse_ruby.existeHypothese?(1))
            chaine += "1"
        else
            chaine += " "
        end
        chaine += "  "
        if (@caseReponse_ruby.existeHypothese?(2))
            chaine += "2"
        else
            chaine += " "
        end
        chaine += "  "
        if (@caseReponse_ruby.existeHypothese?(3))
            chaine += "3"
        else
            chaine += " "
        end
        chaine += "\n"
        if (@caseReponse_ruby.existeHypothese?(4))
            chaine += "4"
        else
            chaine += " "
        end
        chaine += "  "
        if (@caseReponse_ruby.existeHypothese?(5))
            chaine += "5"
        else
            chaine += " "
        end
        chaine += "  "
        if (@caseReponse_ruby.existeHypothese?(6))
            chaine += "6"
        else
            chaine += " "
        end
        chaine += "\n"
        if (@caseReponse_ruby.existeHypothese?(7))
            chaine += "7"
        else
            chaine += " "
        end
        chaine += "  "
        if (@caseReponse_ruby.existeHypothese?(8))
            chaine += "8"
        else
            chaine += " "
        end
        chaine += "  "
        if (@caseReponse_ruby.existeHypothese?(9))
            chaine += "9"
        else
            chaine += " "
        end
    end

end
