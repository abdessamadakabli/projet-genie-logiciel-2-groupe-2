require 'gtk3'
load '../chronometre/Chronometre.rb'
load 'PaveNumeriqueHypothese.rb'
load 'PaveNumeriqueReponse.rb'
load '../menuPrincipal/MenuPrincipale.rb'
load '../options/Option.rb'
load '../nouvellePartie/NouvellePartie.rb'
load '../nouvellePartie/FenetreSelectionGrilles.rb'
load '../aPropos/Apropos.rb'
load '../profil/Profil.rb'
load 'Partie.rb'
load '../nouvellePartie/Pause.rb'
load '../sauvegarde/Sauvegarde.rb'
load '../classement/Classement.rb'

##
#	Cette classe permet d'afficher la fenêtre qui contiendra la grille de kakuro 
#	ainsi que l'ensemble des fonctionnalités qui y sont liées
class Builder < Gtk::Builder
	##
	#	Ce constructeur permet de charger l'interface graphique réalisée avec Glade
	def initialize()
	    super()
		self.add_from_file("../glade/FenetrePartie.glade")
		
		# Creation d'une variable d'instance par composant identifié dans glade
		puts "Création des variables d'instances"

		@Profil = Profil.creer
		@NouvellePartie = NouvellePartie.creer(@Profil)
		@MenuPrincipale = MenuPrincipale.creer
		@Classement = Classement.creer(@MenuPrincipale)
		@Option = Option.creer
		@Apropos = Apropos.creer
		@Partie5 = Partie.creer(@Profil, 5)
		@Partie7 = Partie.creer(@Profil, 7)
		@Partie10 = Partie.creer(@Profil, 10)
		@PaveNumerique = PaveNumeriqueReponse.creer
		@PaveHypothese = PaveNumeriqueHypothese.creer
		@Pause = Pause.creer(@Profil)
		@Sauvegarde = Sauvegarde.creer(@Profil)
		@FenetreSelectionGrilles = FenetreSelectionGrilles.creer(@Profil)

		@listePages = [@NouvellePartie, @MenuPrincipale, @Classement, @Option, @Apropos, @Profil, @Partie5, @Partie7, @Partie10, @Pause, @Sauvegarde, @FenetreSelectionGrilles]

		self.objects.each() { |p| 	
				unless p.builder_name.start_with?("___object")
					puts "\tCreation de la variable d'instance @#{p.builder_name}"
					if p.builder_name.start_with?("PRINCIPAL") then
						@MenuPrincipale.ajouterVariable(p.builder_name.delete_prefix("PRINCIPAL_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PROFIL") then
						@Profil.ajouterVariable(p.builder_name.delete_prefix("PROFIL_"), self[p.builder_name])
					elsif p.builder_name.start_with?("NOUVELLE") then
						@NouvellePartie.ajouterVariable(p.builder_name.delete_prefix("NOUVELLE_"), self[p.builder_name])
					elsif p.builder_name.start_with?("OPTIONS") then
						@Option.ajouterVariable(p.builder_name.delete_prefix("OPTIONS_"), self[p.builder_name])
					elsif p.builder_name.start_with?("APROPOS") then
						@Apropos.ajouterVariable(p.builder_name.delete_prefix("APROPOS_"), self[p.builder_name])
					elsif p.builder_name.start_with?("CHARGER") then
						@Sauvegarde.ajouterVariable(p.builder_name.delete_prefix("CHARGER_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PARTIE5") then
						@Partie5.ajouterVariable(p.builder_name.delete_prefix("PARTIE5_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PARTIE7") then
						@Partie7.ajouterVariable(p.builder_name.delete_prefix("PARTIE7_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PARTIE10") then
						@Partie10.ajouterVariable(p.builder_name.delete_prefix("PARTIE10_"), self[p.builder_name])
					elsif p.builder_name.start_with?("CLASSEMENT") then
						@Classement.ajouterVariable(p.builder_name.delete_prefix("CLASSEMENT_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PAVENUM") then
						@PaveNumerique.ajouterVariable(p.builder_name.delete_prefix("PAVENUM_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PAVEHYPO") then
						@PaveHypothese.ajouterVariable(p.builder_name.delete_prefix("PAVEHYPO_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PAUSE") then
						@Pause.ajouterVariable(p.builder_name.delete_prefix("PAUSE_"), self[p.builder_name])
					elsif p.builder_name.start_with?("SELECTION") then
						@FenetreSelectionGrilles.ajouterVariable(p.builder_name.delete_prefix("SELECTION_"), self[p.builder_name])
					end
					instance_variable_set("@#{p.builder_name}".intern, self[p.builder_name]) 
				end
		}

		@MenuPrincipale.ajouterVariable("ChargerPartie", @Sauvegarde)
		@MenuPrincipale.ajouterVariable("NouvellePartie", @NouvellePartie)
		@MenuPrincipale.ajouterVariable("Profil", @Profil)
		@MenuPrincipale.ajouterVariable("Option", @Option)
		@MenuPrincipale.ajouterVariable("Apropos", @Apropos)
		@MenuPrincipale.ajouterVariable("Classement", @Classement)
		@NouvellePartie.ajouterVariable("MenuPrincipale", @MenuPrincipale)
		@NouvellePartie.ajouterVariable("Partie5", @Partie5)
		@NouvellePartie.ajouterVariable("Partie7", @Partie7)
		@NouvellePartie.ajouterVariable("Partie10", @Partie10)
		@NouvellePartie.ajouterVariable("FenetreSelectionGrilles", @FenetreSelectionGrilles)
		@NouvellePartie.ajouterVariable("Profil", @Profil)
		@Sauvegarde.ajouterVariable("MenuPrincipale", @MenuPrincipale)
		@Sauvegarde.ajouterVariable("Partie5", @Partie5)
		@Option.ajouterVariable("MenuPrincipale", @MenuPrincipale)
		@Option.ajouterVariable("NouvellePartie", @NouvellePartie)
		@Option.ajouterVariable("Apropos", @Apropos)
		@Option.ajouterVariable("Profil", @Profil)
		@Option.ajouterVariable("Pause", @Pause)
		@Option.ajouterVariable("Partie5", @Partie5)
		@Option.ajouterVariable("Partie7", @Partie7)
		@Option.ajouterVariable("Partie10", @Partie10)
		@Option.ajouterVariable("ChargerPartie", @Sauvegarde)
		@Apropos.ajouterVariable("MenuPrincipale", @MenuPrincipale)
		@Profil.ajouterVariable("MenuPrincipale", @MenuPrincipale)
		@Profil.ajouterVariable("Option", @Option)
		@Partie5.ajouterVariable("PaveHypothese", @PaveHypothese)
		@Partie5.ajouterVariable("PaveNumerique", @PaveNumerique)
		@Partie5.ajouterVariable("Pause", @Pause)
		@Partie5.ajouterVariable("FenetreSelectionGrilles", @FenetreSelectionGrilles)
		@Partie5.ajouterVariable("masquerbloc", @PARTIE_masquerbloc)
		@Partie5.ajouterVariable("MenuPrincipale", @MenuPrincipale)
		@Partie7.ajouterVariable("PaveHypothese", @PaveHypothese)
		@Partie7.ajouterVariable("FenetreSelectionGrilles", @FenetreSelectionGrilles)
		@Partie7.ajouterVariable("PaveNumerique", @PaveNumerique)
		@Partie7.ajouterVariable("Pause", @Pause)
		@Partie7.ajouterVariable("masquerbloc", @PARTIE_masquerbloc)
		@Partie10.ajouterVariable("FenetreSelectionGrilles", @FenetreSelectionGrilles)
		@Partie10.ajouterVariable("PaveHypothese", @PaveHypothese)
		@Partie10.ajouterVariable("PaveNumerique", @PaveNumerique)
		@Partie10.ajouterVariable("Pause", @Pause)
		@Partie10.ajouterVariable("masquerbloc", @PARTIE_masquerbloc)
		@Pause.ajouterVariable("MenuPrincipale", @MenuPrincipale)
		@Pause.ajouterVariable("Option", @Option)
		@FenetreSelectionGrilles.ajouterVariable("NouvellePartie", @NouvellePartie)
		@FenetreSelectionGrilles.ajouterVariable("Partie5", @Partie5)
		@FenetreSelectionGrilles.ajouterVariable("Partie7", @Partie7)
		@FenetreSelectionGrilles.ajouterVariable("Partie10", @Partie10)


		@listePages.each { |p|
			p.ajouterVariable("fenetre", @PRINCIPAL_fenetreMenuPrincipale)
		}
		@PaveNumerique.ajouterVariable("fenetre", @PAVENUM_fenetrePaveNumerique)
		@PaveHypothese.ajouterVariable("fenetre", @PAVEHYPO_fenetrePavehypothese)
		

		provider = Gtk::CssProvider.new

        provider.load_from_path('../style/couleur.css')
    
    
      	Gtk::StyleContext.add_provider_for_screen(Gdk::Screen.default, provider, Gtk::StyleProvider::PRIORITY_APPLICATION)

		# On connecte les signaux aux méthodes (qui doivent exister)

		@listePages.each { |p|
			p.initialiserSignaux
		}
		@PaveNumerique.initialiserSignaux
		@PaveHypothese.initialiserSignaux
		
		@Option.initOptions
		@Profil.getTousLesProfils

		@PRINCIPAL_fenetreMenuPrincipale.window_position = Gtk::WindowPosition::CENTER_ALWAYS
		@PAVENUM_fenetrePaveNumerique.resizable=(false)
		@PAVEHYPO_fenetrePavehypothese.resizable=(false)
		@PRINCIPAL_fenetreMenuPrincipale.resizable=(false)
		@PRINCIPAL_fenetreMenuPrincipale.title=("Kakuro")
		@PRINCIPAL_fenetreMenuPrincipale.signal_connect('destroy') { 
			puts "Au Revoir !!!"; Gtk.main_quit 
		}

		@PRINCIPAL_fenetreMenuPrincipale.show
	end
end
# On lance l'application
builder = Builder.new()
Gtk.main
