##
# Classe pour le bloc affichant des informations sur la partie en cours
class Info

    attr_writer :tentative, :labelMode, :labelDifficulte
    attr_reader :mode
    ##
    #   Permet de créer l'ensemble des informations relatifs à la partie de jeu
    #   @param mode Le mode de jeu : Tutoriel Classique Compétition Perfection
    #   @param difficulte La difficulté de la grille : Facile Moyen Difficile
    #   @param labelMode L'élément visuelle du jeu qui affiche le mode
    #   @param labelDifficulte L'élément visuelle du jeu qui affiche la difficulté
    #   @param labelTentative L'élément visuelle du jeu qui affiche le nombre de tentatives du joueur
    def Info.creer(mode, difficulte, labelMode, labelDifficulte, labelTentative)
        new(mode, difficulte, labelMode, labelDifficulte, labelTentative)
    end 
    
    ##
    #   Constructeur des informations relatifs à la partie de jeu
    #   @param mode Le mode de jeu : Tutoriel Classique Compétition Perfection
    #   @param difficulte La difficulté de la grille : Facile Moyen Difficile
    #   @param labelMode L'élément visuelle du jeu qui affiche le mode
    #   @param labelDifficulte L'élément visuelle du jeu qui affiche la difficulté
    #   @param labelTentative L'élément visuelle du jeu qui affiche le nombre de tentatives du joueur
    def initialize(mode, difficulte, labelMode, labelDifficulte, labelTentative)
        @mode = mode
        @difficulte = difficulte
        @tentative = 0
        @labelTentative = labelTentative
        @labelMode = labelMode
        @labelDifficulte = labelDifficulte

        @labelMode.set_label(@mode)
        @labelDifficulte.set_label(@difficulte)
    end

    ##
    #   Permet d'incrementer le nombre de tentative et de l'afficher à l'écran
    def incr_Tentative()
        @tentative += 1
        @labelTentative.set_label(@tentative.to_s)
    end

end