require "../grille/Grille.rb"
require "../partie/Informations.rb"
require "../cases/Case.rb"
require "../cases/CaseNeutre.rb"
require "../cases/CaseReponse.rb"
require "../cases/CaseSomme.rb"

##
#   Permet de géréer le déroulement d'une partie de kakuro
#   Elle effectue la gestion des signaux et des actions du joueur
class Partie < Page

    attr_accessor :profil, :chronometre
    ##
    #   redéfinition d'initialize
    #   @param profil du joueur qui sera utilisé
    #   @param taille de la grille de jeu
    def initialize(profil, taille)
        super()
        @infosLangue = nil
        @taille = taille
        @profil = profil
        @is_Eraser = false
        @is_Pencil = false
        @nb_Aide_Used = 0
        @nb_Verif = 0 
        @tutoActif = false
        @depassementSommeActif = false
        @threadTuto = nil

    end
    ##
    # New étant privée on la rédéfinit
    # @param profil le profil
    # @param taille la taille de la grille
    def Partie.creer(profil, taille)
        new(profil, taille)
    end

    ##
    #   Permet de créer un chronometre
    def creerChronometre
        @chronometre = Chronometre.creer(@lblChronometre, @boutonNouvellePartie, @boutonPause, @tutoActif)
    end

    ##
    #   Permet d'initialiser les signaux (Le bouton de Menu de pause et de relance d'une nouvelle grille)
    def initialiserSignaux()
        @fenetreKakuro.remove(@base)
        @boutonMenu.signal_connect('clicked'){
            if (!@tutoActif) then
                @chronometre.pause()
                @profil.grille.tempsChrono = [@chronometre.heures, @chronometre.minutes, @chronometre.secondes]
                cacher
                @Pause.setPartie(self)
                @Pause.afficher
            end
        }

        @boutonNouvellePartie.signal_connect('clicked'){
            self.cacher()
            @FenetreSelectionGrilles.afficher()
        }
        
        connexionSignauxBarreOutils()
    end

    ##
    #   Permet de regénérer la grille Gtk de Kakuro
    def refreshBtn()
        while true do
            if @grille.get_child_at(1,0) != nil then
                @grille.remove_row(0)
            else
                break
            end
        end

        for i in 1..@taille do
            @grille.attach(Gtk::Label.new(i.to_s), 0, i, 1, 1)
            @grille.attach(Gtk::Label.new(('A'.ord() + i - 1).chr), i, 0, 1, 1)
        end
    
        @grilleCasesGlade = Array.new(@taille){Array.new(@taille)}
        for j in 1..@taille do
            for i in 1..@taille do
                @grilleCasesGlade[j-1][i-1] = Gtk::Button.new()
                @grille.attach(@grilleCasesGlade[j-1][i-1], i, j, 1, 1)
            end
        end
        @fenetre.show_all
    end
    ##
    #   Fonctionnement de l'aide du dépassement de somme qui crée les infobulles en jeu
    #   @param caseSomme la case sur laquelle on veut afficher les possibles dépassements de somme
    #   @param sens le sens horizontal ou vertical que l'on souhaite afficher
    def actionDepassementSommeBouton(caseSomme, sens)
        if(caseSomme.nbCasesHorizontales > 0 && sens =="h")
            affichage = caseSomme.afficherDepassementSomme(caseSomme.detecterDepassementSommeHorizontale, "h")
            label = Gtk::Label.new(affichage)
            popover = creerPopover(@boutonDepassementSomme, label, :bottom)
            popover.show
        end
        if(caseSomme.nbCasesVerticales > 0 && sens =="v")
            affichage = caseSomme.afficherDepassementSomme(caseSomme.detecterDepassementSommeVerticale, "v")
            label = Gtk::Label.new(affichage)
            popover = creerPopover(@boutonDepassementSomme, label, :bottom)
            popover.show
        end
        return popover
    end

    ##
    #   Permet d'initialiser les signaux des boutons d'une ligne de la grille
    #   @param indiceLigneCasesBoutons la ligne à traiter
    def initialiserLignesCasesBoutons(indiceLigneCasesBoutons)
        @grilleCasesGlade[indiceLigneCasesBoutons].each_with_index{ |c,index|
            if (@grilleJeu.getCase(indiceLigneCasesBoutons,index).class() == CaseReponse)
                c.signal_connect('button_press_event') {|widget, event|
                if (@chronometre.getPause() == false)
                    nettoyerGrille()
                    if (event.button == 1) # Gestion du clic gauche
                        if @is_Eraser == true # Gestion de la gomme
                            c.set_label("")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerReponse()
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerHypotheses()
                            c.set_name("white_case_rep")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "white_case_rep"
                        elsif @is_Pencil == true # Gestion du crayon
                            if (@grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur == "white_case_rep")
                                c.set_name("green_case")
                                @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "green_case"
                            else
                                c.set_name("white_case_rep")
                                @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "white_case_rep"
                            end
                        else # Gestion du pavé numérique REPONSE
                            c.set_name("yellow_case")
                            if (@info.mode == "Perfection")
                                @PaveNumerique.afficher(@grilleJeu.getCase(indiceLigneCasesBoutons,index), c, @grilleJeu)
                            else
                                @PaveNumerique.afficher(@grilleJeu.getCase(indiceLigneCasesBoutons,index), c, @grilleJeu)
                            end
                        end
                    elsif (event.button == 3) # Gestion du clic droit
                        if (@is_Eraser == true) # Gestion de la gomme
                            c.set_label("")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerReponse()
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerHypotheses()
                            c.set_name("white_case_rep")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "white_case_rep"
                        elsif (@is_Pencil == true) # Gestion du crayon
                            if (@grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur == "white_case_rep")
                                c.set_name("yellow_case")
                                @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "yellow_case"
                            else
                                c.set_name("white_case_rep")
                                @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "white_case_rep"
                            end
                        else # Gestion du pavé numérique HYPOTHESE 
                            c.set_name("orange_case")
                            @PaveHypothese.afficher(@grilleJeu.getCase(indiceLigneCasesBoutons,index), c, @grilleJeu)
                        end
                    end
                end
                }
            elsif (@grilleJeu.getCase(indiceLigneCasesBoutons,index).class() == CaseSomme)
                c.signal_connect('button_press_event') {|widget, event|
                    if (@chronometre.getPause() == false)
                        if (event.button == 3) # Le clic droit sur une case comme affiche la somme horizontale
                            if(!@depassementSommeActif)
                                combinaisonsAffichage = @grilleJeu.getCase(indiceLigneCasesBoutons,index).combinaisonsAffichage(@grilleJeu.getCase(indiceLigneCasesBoutons,index).combinaisonPossiblesSommeHorizontale(), "h")
                                sommeHorizontale = "Somme horizontale actuelle : #{@grilleJeu.getCase(indiceLigneCasesBoutons,index).calculerSommeActuelleHorizontale().to_s()}\n"
                                label = Gtk::Label.new( sommeHorizontale + "\n" + combinaisonsAffichage)
                                popover = creerPopover(@grilleCasesGlade[indiceLigneCasesBoutons][index], label, :bottom)
                                popover.show
                            else
                                actionDepassementSommeBouton(@grilleJeu.grille[indiceLigneCasesBoutons][index], "h")
                            end
                        end
                        if (event.button == 1) # Le clic gauche sur une case comme affiche la somme verticale
                            if(!@depassementSommeActif)
                                combinaisonsAffichage = @grilleJeu.getCase(indiceLigneCasesBoutons,index).combinaisonsAffichage(@grilleJeu.getCase(indiceLigneCasesBoutons,index).combinaisonPossiblesSommeVerticale(), "v")
                                #print("(#{indiceLigneCasesBoutons},#{index}) : #{combinaisonsAffichage}\n")
                                sommeVerticale = "Somme verticale actuelle : #{@grilleJeu.getCase(indiceLigneCasesBoutons,index).calculerSommeActuelleVerticale().to_s()}\n"
                                label = Gtk::Label.new(sommeVerticale +  "\n" + combinaisonsAffichage)
                                popover = creerPopover(@grilleCasesGlade[indiceLigneCasesBoutons][index], label, :bottom)
                                popover.show
                            else
                                actionDepassementSommeBouton(@grilleJeu.grille[indiceLigneCasesBoutons][index], "v")
                            end
                        end
                    end
                }
            end
        }
        # -----------
    end

    ##
    #   Permet d'intiialiser les boutons Gtk3 en fonction de la grille du jeu
    def initialiserBoutonsCasesGlade()
        refreshBtn
        chaine = String.new()
        for i in 0..@taille-1
            for j in 0..@taille-1
                if (@grilleJeu.getCase(i,j).class() == CaseReponse)                    
                    if(@grilleJeu.getCase(i,j).reponse != -1)
                        @grilleCasesGlade[i][j].set_name("white_case_rep")
                        @grilleJeu.grille[i][j].couleur = "white_case_rep"
                        @grilleCasesGlade[i][j].set_label("#{@grilleJeu.getCase(i,j).reponse}")
                    else
                        if(@grilleJeu.getCase(i,j).existeHypotheses())
                            @grilleCasesGlade[i][j].set_name("white_case_hyp")
                            @grilleJeu.grille[i][j].couleur = "white_case_hyp"
                            @grilleCasesGlade[i][j].set_label(affichageHypothese(@grilleJeu.grille[i][j]))
                        else
                            @grilleCasesGlade[i][j].set_name("white_case_rep")
                            @grilleJeu.grille[i][j].couleur = "white_case_rep"
                            @grilleCasesGlade[i][j].set_label("")
                        end
                    end
                elsif (@grilleJeu.getCase(i,j).class() == CaseNeutre)
                    @grilleCasesGlade[i][j].set_name("blue_case")
                    @grilleCasesGlade[i][j].set_label("")
                elsif (@grilleJeu.getCase(i,j).class() == CaseSomme)
                    @grilleCasesGlade[i][j].set_name("blue_case")
                    @grilleCasesGlade[i][j].set_label("")
                    if(@grilleJeu.getCase(i,j).sommeHorizontale == 0)
                        sommeHorizontale = ""
                    else
                        sommeHorizontale = @grilleJeu.getCase(i,j).sommeHorizontale
                    end
                    if(@grilleJeu.getCase(i,j).sommeVerticale == 0)
                        sommeVerticale = ""
                    else
                        sommeVerticale = @grilleJeu.getCase(i,j).sommeVerticale
                    end
                    if(sommeHorizontale == "" and sommeVerticale == "")
                        @grilleCasesGlade[i][j].set_label("")
                    else
                        @grilleCasesGlade[i][j].set_label("\n            " + sommeHorizontale.to_s + "\n     " + sommeVerticale.to_s)
                    end
                end
            end
        end
        @grilleJeu.setGrilleGlade(@grilleCasesGlade)
        if (!@tutoActif) then 
            for indiceLigneCasesBoutons in 0..@taille-1 do
                initialiserLignesCasesBoutons(indiceLigneCasesBoutons)
            end
        end
    end

    ##
    #   Permet de vérifier une ligne de la grille
    #   @param indiceLigne indice de la ligne
    #   @param a_gagne résultat si le joueur est gagnant ou non
    def verifierLignesGrille(indiceLigne, a_gagne)
        @grilleCasesGlade[indiceLigne].each_with_index{ |c,index|
            if (@grilleJeu.getCase(indiceLigne,index).class() == CaseReponse)
                if ( @grilleJeu.getCase(indiceLigne,index).verifierReponse() == true)
                    c.set_name("green_case")   # On indique au joueur que la case est bien remplie avec la couleur Verte
                elsif (@grilleJeu.getCase(indiceLigne,index).existeReponse() == true)
                    a_gagne = false             # Si l'on trouve au moins une erreur dans la grille de Kakuro alors le joueur ne peut pas avoir gagné
                    c.set_name("red_case")  # On indique au joueur son erreur sur la case en la remplissant de Rouge
                else
                    a_gagne = false
                end
            end
        }
        return a_gagne
    end

    ##
    #   Permet de vérifier la grille du jeu
    def verifier_Grille()
        a_gagne = true       # Par défaut, si l'on ne trouve aucunes erreurs le joueur aura gagné
        for indiceLignes in 0..@taille-1          
            a_gagne = verifierLignesGrille(indiceLignes, a_gagne)
        end
        @info.incr_Tentative()
        return a_gagne
    end

     ##
     #  Permet de connecter les signaux de la barre d'outil et de la barre d'aide
     def connexionSignauxBarreOutils()
        @boutonBombe.set_name("white_case")
        @boutonGomme.set_name("white_case")
        @boutonAvant.set_name("white_case")
        @boutonApres.set_name("white_case")
        @boutonPoubelle.set_name("white_case")
        @boutonCrayon.set_name("white_case")
        @blocmagique.set_name("white_çase")
        
        @boutonVerifierGrille.signal_connect('clicked'){
            if ((@chronometre.getPause() == false) && (!@tutoActif)) then
                popover = creerPopover(@boutonVerifierGrille, Gtk::Label.new("Vérifier la grille"), :bottom)
                popover.show
                a_gagne = verifier_Grille()
                if (a_gagne == true)
                    @chronometre.pause()
                    gagne()
                    Thread.new{
                        if (getMode == "Competition" || getMode == "Perfection") # Permet de jouer une petite animation lors du gains de trophées
                            case @nbTropheesGagne
                            when 1
                                @imagetrophee.pixbuf_animation = GdkPixbuf::PixbufAnimation.new '../glade/img/victoire/trophees_1.gif'
                                @imagetrophee.show()
                            when 2
                                @imagetrophee.pixbuf_animation = GdkPixbuf::PixbufAnimation.new '../glade/img/victoire/trophees_2.gif'
                                @imagetrophee.show()
                            when 3
                                @imagetrophee.pixbuf_animation = GdkPixbuf::PixbufAnimation.new '../glade/img/victoire/trophees_3.gif'
                                @imagetrophee.show()
                            when 4
                                @imagetrophee.pixbuf_animation = GdkPixbuf::PixbufAnimation.new '../glade/img/victoire/trophees_4.gif'
                                @imagetrophee.show()
                            when 5
                                @imagetrophee.pixbuf_animation = GdkPixbuf::PixbufAnimation.new '../glade/img/victoire/trophees_5.gif'
                                @imagetrophee.show()
                            when 6
                                @imagetrophee.pixbuf_animation = GdkPixbuf::PixbufAnimation.new '../glade/img/victoire/trophees_6.gif'
                                @imagetrophee.show()
                            when 7
                                @imagetrophee.pixbuf_animation = GdkPixbuf::PixbufAnimation.new '../glade/img/victoire/trophees_7.gif'
                                @imagetrophee.show()
                            end
                        end
                        i = 10
                        while (i >= 0)
                            @bloctxt.set_label("Felicitation ! Vous avez termines la grille en : #{@chronometre.heures} heure(s) #{@chronometre.minutes} minute(s) #{@chronometre.secondes} seconde(s).\nFermeture de la grille dans #{i} secondes.")
                            sleep(1)
                            i = i - 1                            
                        end
                        @imagetrophee.pixbuf_animation = nil
                        @imagetrophee.hide()
                        cacher
                        @FenetreSelectionGrilles.afficher()
                    }       
                else
                    @nb_Verif += 1
                end
            end            
        }

        @boutonBombe.signal_connect('clicked'){ # Bombe
            if ((@chronometre.getPause() == false) && (!@tutoActif)) then
                popover = creerPopover(@boutonBombe, Gtk::Label.new("Effacer toutes les hypothèses"), :bottom)
                popover.show
                @grilleJeu.effacerToutesLesHypotheses()
            end
        }
        
        @boutonGomme.signal_connect('clicked'){ # Gomme
            if ((@chronometre.getPause() == false) && (!@tutoActif)) then
                popover = creerPopover(@boutonGomme, Gtk::Label.new("Effacer une réponse/hypothèse"), :bottom)
                popover.show
                @is_Pencil = false
                @boutonCrayon.set_name("white_case")
                if (@is_Eraser == false)
                    @is_Eraser = true
                    @boutonGomme.set_name("yellow_case")
                else
                    @is_Eraser = false
                    @boutonGomme.set_name("white_case")
                end
            end
        }

        @boutonAvant.signal_connect('clicked'){ # Undo
            if ((@chronometre.getPause() == false) && (!@tutoActif)) then
                popover = creerPopover(@boutonAvant, Gtk::Label.new("Revenir un coup en arrière."), :bottom)
                popover.show
                @grilleJeu.historique.undo()
            end
        }
        
        @boutonApres.signal_connect('clicked'){ # Redo
            if ((@chronometre.getPause() == false) && (!@tutoActif)) then
                popover = creerPopover(@boutonApres, Gtk::Label.new("Revenir un coup en avant."), :bottom)
                popover.show
                @grilleJeu.historique.redo()
            end
        }

        @boutonPoubelle.signal_connect('clicked'){ # Poubelle
            if ((@chronometre.getPause() == false) && (!@tutoActif)) then
                popover = creerPopover(@boutonPoubelle, Gtk::Label.new("Effacer toutes les réponses"), :bottom)
                popover.show
                @grilleJeu.effacerToutesLesReponses()
            end
        }
        
        @boutonCrayon.signal_connect('clicked'){ # Crayon couleur
            if ((@chronometre.getPause() == false) && (!@tutoActif)) then
                popover = creerPopover(@boutonCrayon, Gtk::Label.new("Colorier une case (Vert clic gauche / Jaune clic droit)"), :bottom)
                popover.show
                @is_Eraser = false
                @boutonGomme.set_name("white_case")
                if (@is_Pencil == false)
                    @is_Pencil = true
                    @boutonCrayon.set_name("yellow_case")
                else
                    @is_Pencil = false
                    @boutonCrayon.set_name("white_case")
                end
            end
        }

        @blocmagique.signal_connect('clicked'){ # Afficher le tableau des blocs magiques
            for i in 0..@grilleJeu.taille-1
                for j in 0..@grilleJeu.taille-1
                    if (@grilleJeu.getCase(i,j).class() == CaseReponse && @grilleJeu.getCase(i,j).existeHypotheses())
                        @grilleCasesGlade[i][j].set_name("white_case_hyp")
                    elsif (@grilleJeu.getCase(i,j).class() == CaseReponse )
                        @grilleCasesGlade[i][j].set_name("white_case_rep")
                    end
                end
            end
            popover = creerPopover(@blocmagique, Gtk::Label.new("Afficher la liste des combinaisons formanat des blocs magiques."), :bottom)
            popover.show
            if ((@chronometre.getPause() == false ) && (!@tutoActif))
                @blocmagique.set_name("white_case")
                if (@is_MagicBloc_printed == false)
                    @is_MagicBloc_printed = true
                    @bloc.show()
                    @blocmagique.set_name("yellow_case")
                else
                    @is_MagicBloc_printed = false
                    @bloc.hide()
                    @blocmagique.set_name("white_case")
                end
            end
        }

        @Afficher_bloc.signal_connect('clicked'){ # Bloc Magique
            if ((@chronometre.getPause() == false) && (!@tutoActif))
                @depassementSommeActif = false
                @boutonDepassementSomme.set_name("aff_depassementSomme")
                @bloctxt.set_label("Afficher les cases formant un bloc magique. (une seule combinaison\npour les résoudre).A chaque clic un nouveau bloc magique est proposé.")
                @nb_Aide_Used += 1
                isMagic = false
                for i in 0..@grilleJeu.taille-1
                    for j in 0..@grilleJeu.taille-1
                        if (@grilleJeu.getCase(i,j).class() == CaseSomme && isMagic == false)
                            if (@grilleJeu.getCase(i,j).isBlocMagicHorizontale == false && @grilleJeu.getCase(i,j).estBlocMagiqueSommeHorizontale?() && isMagic == false)
                                isMagic = true
                                @grilleJeu.getCase(i,j).isBlocMagicHorizontale = true
                                for y in @grilleJeu.getCase(i,j).y+1..@grilleJeu.getCase(i,j).y+ @grilleJeu.getCase(i,j).nbCasesHorizontales
                                    @grilleCasesGlade[i][y].set_name("bloc_magic")
                                end
                            elsif (@grilleJeu.getCase(i,j).isBlocMagicVerticale == false && @grilleJeu.getCase(i,j).estBlocMagiqueSommeVerticale?() && isMagic == false)
                                isMagic = true
                                @grilleJeu.getCase(i,j).isBlocMagicVerticale = true
                                for x in @grilleJeu.getCase(i,j).x+1..@grilleJeu.getCase(i,j).x+ @grilleJeu.getCase(i,j).nbCasesVerticales
                                    @grilleCasesGlade[x][j].set_name("bloc_magic")
                                end
                            end
                        end
                    end
                end
            end
        }

        @Afficher_pair.signal_connect('clicked'){ # Aide des paires
            if((!@tutoActif))
                @depassementSommeActif = false
                @boutonDepassementSomme.set_name("aff_depassementSomme")
                @bloctxt.set_label("Enlève des hypothèses les suggestions inutiles en appliquant la règles de paires,\ntriplets et quadruplets.Si une case somme possède deux cases différents\n ayant des hypothèses de plus de 2 chiffres similaires, alors on efface des\nautres cases réponses ces chiffres.")
                @nb_Aide_Used += 1
                for k in 2..9
                    for i in 0..@grilleJeu.taille-1
                        for j in 0..@grilleJeu.taille-1
                            if(@grilleJeu.getCase(i,j).class() == CaseSomme)
                                if(@grilleJeu.getCase(i,j).nbCasesHorizontales > 0)
                                    @grilleJeu.getCase(i,j).occurrencesHorizontales(k)
                                    nbCases = @grilleJeu.getCase(i,j).nbCasesHorizontales
                                    for m in @grilleJeu.grille[i][j].y+1..nbCases
                                        if(@grilleJeu.grille[i][m].class() == CaseReponse)
                                            if(!@grilleJeu.grille[i][m].existeReponse)
                                                @grilleCasesGlade[i][m].set_label(affichageHypothese(@grilleJeu.grille[i][m]))
                                                @grilleCasesGlade[i][m].set_name("white_case_hyp")
                                                @grilleJeu.grille[i][m].couleur = "white_case_hyp"
                                            end
                                        end
                                    end
                                end
                                if(@grilleJeu.getCase(i,j).nbCasesVerticales > 0)
                                    @grilleJeu.getCase(i,j).occurrencesVerticales(k)
                                    nbCases = @grilleJeu.getCase(i,j).nbCasesVerticales
                                    for m in @grilleJeu.grille[i][j].x+1..nbCases
                                        if(@grilleJeu.grille[i][m].class() == CaseReponse)
                                            if(!@grilleJeu.grille[i][m].existeReponse)
                                                @grilleCasesGlade[m][j].set_label(affichageHypothese(@grilleJeu.grille[m][j]))
                                                @grilleCasesGlade[m][j].set_name("white_case_hyp")
                                                @grilleJeu.grille[m][j].couleur = "white_case_hyp"
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        }

        @boutonDepassementSomme.signal_connect('clicked'){ # Aide dépassement de somme
            if(!@tutoActif)
                @bloctxt.set_label("Permet de signaler au joueur lorsqu'il clique sur une case somme les combinaisons \nd'hypothèses qui dépassent la valeur attendue par la case somme.")
                @nb_Aide_Used += 1
                @depassementSommeActif = @depassementSommeActif == false
                if(@depassementSommeActif)
                    @boutonDepassementSomme.set_name("aff_depassementSommeActif")
                else
                    @boutonDepassementSomme.set_name("aff_depassementSomme")
                    @bloctxt.set_label("")
                end
            end
        }

        @Afficherintersection.signal_connect('clicked'){ # Aide intersections
            for i in 0..@grilleJeu.taille-1
                for j in 0..@grilleJeu.taille-1
                    if (@grilleJeu.getCase(i,j).class() == CaseReponse && @grilleJeu.getCase(i,j).existeHypotheses())
                        @grilleCasesGlade[i][j].set_name("white_case_hyp")
                    elsif (@grilleJeu.getCase(i,j).class() == CaseReponse )
                        @grilleCasesGlade[i][j].set_name("white_case_rep")
                    end
                end
            end
            if (!@tutoActif) then
                @depassementSommeActif = false
                @boutonDepassementSomme.set_name("aff_depassementSomme")
                @bloctxt.set_label("Permet de réaliser l'intersection entre deux cases somme en \naffichant les hypothèses qui sont en communs.")
                @nb_Aide_Used += 1
                ok = nil
                i = 0
                while i < @grilleJeu.taille && ok == nil do
                    j = 0
                    while j < @grilleJeu.taille && ok == nil do
                        if @grilleJeu.getCase(i,j).class() == CaseSomme then
                            ok = @grilleJeu.getCase(i,j).determinerIntersections()
                            if (ok != nil) then
                                ok.effacerReponse()
                                @grilleCasesGlade[ok.getX][ok.getY].set_label(affichageHypothese(ok))
                                @grilleCasesGlade[ok.getX][ok.getY].set_name("intersection_case")
                                ok.couleur = "intersection_case"
                            end
                        end
                        j += 1
                    end
                    i += 1
                end
            end
        }

    end

    ##
    #   Permet de nettoyer les couleurs de la grille
    def nettoyerGrille()
        for i in 0..@grilleJeu.taille-1
            for j in 0..@grilleJeu.taille-1
                if (@grilleJeu.getCase(i,j).class() == CaseReponse && @grilleJeu.getCase(i,j).existeReponse())
                    @grilleCasesGlade[i][j].set_name("white_case_rep")
                elsif (@grilleJeu.getCase(i,j).class() == CaseReponse && @grilleJeu.getCase(i,j).existeHypotheses())
                    @grilleCasesGlade[i][j].set_name("white_case_hyp")
                end
            end
        end
    end

    ##
    #   Permet de récupérer le mode de jeu
    def getMode()
        return @etiquetteValeurMode.text
    end

    ##
    #   Permet de récupérer la difficulté de la partie
    def getDifficulte()
        return @etiquetteValeurDifficulte.text
    end

    ##
    #   Méthode permettant de calculer le nombre de trophées gagnés par le joueur et qui met à jour son profil
    def gagne()
        @nbTropheesGagne = @nb_Aide_Used + @nb_Verif
        case getMode
        when "Competition"
            case getDifficulte
            when "Facile"
                @profil.nbPartiesCompetitif[0] += 1
                if (@nbTropheesGagne <= 4)
                    @nbTropheesGagne = 3
                elsif (@nbTropheesGagne <= 6)
                    @nbTropheesGagne = 2
                else
                    @nbTropheesGagne = 1
                end
                @profil.nbTropheesCompetitif[0] += @nbTropheesGagne
            when "Moyen"
                @profil.nbPartiesCompetitif[1] += 1
                if (@nbTropheesGagne <= 1)
                    @nbTropheesGagne = 5
                elsif (@nbTropheesGagne <= 2)
                    @nbTropheesGagne = 4
                elsif (@nbTropheesGagne <= 3)
                    @nbTropheesGagne = 3
                elsif (@nbTropheesGagne <= 4)
                    @nbTropheesGagne = 2
                else
                    @nbTropheesGagne = 1
                end
                @profil.nbTropheesCompetitif[1] += @nbTropheesGagne
            when "Difficile"
                @profil.nbPartiesCompetitif[2] += 1
                if (@nbTropheesGagne == 0)
                    @nbTropheesGagne = 7
                elsif (@nbTropheesGagne <= 1)
                    @nbTropheesGagne = 6
                elsif (@nbTropheesGagne <= 2)
                    @nbTropheesGagne = 5
                elsif (@nbTropheesGagne <= 3)
                    @nbTropheesGagne = 4
                elsif (@nbTropheesGagne <= 4)
                    @nbTropheesGagne = 3
                elsif (@nbTropheesGagne <= 5)
                    @nbTropheesGagne = 2
                else
                    @nbTropheesGagne = 1
                end
                @profil.nbTropheesCompetitif[2] += @nbTropheesGagne
            end
            @profil.sauvegarderProfil()
        when "Perfection"
            case @partie.getDifficulte
            when "Facile"
                @profil.nbPartiesPerfection[0] += 1
                if (@nbTropheesGagne <= 4)
                    @nbTropheesGagne = 3
                elsif (@nbTropheesGagne <= 6)
                    @nbTropheesGagne = 2
                else
                    @nbTropheesGagne = 1
                end
                @profil.nbTropheesPerfection[0] += @nbTropheesGagne
            when "Moyen"
                @profil.nbPartiesPerfection[1] += 1
                if (@nbTropheesGagne <= 1)
                    @nbTropheesGagne = 5
                elsif (@nbTropheesGagne <= 2)
                    @nbTropheesGagne = 4
                elsif (@nbTropheesGagne <= 3)
                    @nbTropheesGagne = 3
                elsif (@nbTropheesGagne <= 4)
                    @nbTropheesGagne = 2
                else
                    @nbTropheesGagne = 1
                end
                @profil.nbTropheesPerfection[1] += @nbTropheesGagne
            when "Difficile"
                @profil.nbPartiesPerfection[2] += 1
                if (@nbTropheesGagne == 0)
                    @nbTropheesGagne = 7
                elsif (@nbTropheesGagne <= 1)
                    @nbTropheesGagne = 6
                elsif (@nbTropheesGagne <= 2)
                    @nbTropheesGagne = 5
                elsif (@nbTropheesGagne <= 3)
                    @nbTropheesGagne = 4
                elsif (@nbTropheesGagne <= 4)
                    @nbTropheesGagne = 3
                elsif (@nbTropheesGagne <= 5)
                    @nbTropheesGagne = 2
                else
                    @nbTropheesGagne = 1
                end
                @profil.nbTropheesPerfection[2] += @nbTropheesGagne
            end
            @profil.sauvegarderProfil()
        end
        
    end

    ##
    #   Methode appelée lorsque le joueur a perdu en mode perfection afin de fermer le jeu
    def perdu()
        if (@chronometre.getPause() == false) then
            @chronometre.pause()
            thr = Thread.new{
                i = 10
                while (i >= 0) do
                    @bloctxt.set_label("Perdu ! Vous avez joues pendant : #{@chronometre.heures} heure(s) #{@chronometre.minutes} minute(s) #{@chronometre.secondes} seconde(s).\nFermeture de la grille dans #{i} secondes.")
                    sleep(1)
                    i = i - 1                            
                end
                self.cacher()
                @FenetreSelectionGrilles.afficher()
                Thread.exit()
            }                 
        end      
    end

    ##
    #   Permet de créer un message attaché aux boutons
    def creerPopover(parent, child, pos)
        popover = Gtk::Popover.new(parent)
        popover.position = pos
        popover.add(child)
        child.margin = 6
        child.show
        return popover
    end

    ##
    #   Tutoriel du jeu (Script automatique qui enseigne aux joueurs les bases du jeu)
    def tutoriel()
        suivant = 0
        ancienName = ""
        tempo = 0.5
        @barreDeProgression.set_name("progressBar")
        @barreDeProgression.visible = true
        @barreDeProgression.fraction = 0
        #Effacer les hypothèses
        @threadTuto = Thread.new{
            labelsBoutonsOutils = ["Effacer toutes les hypothèses", "Effacer une réponse/hypothèse", "Undo", "Redo", "Effacer toutes les réponses", "Colorier une case", "Afficher le bloc magique"]
            boutonsOutils = [@boutonBombe, @boutonGomme, @boutonAvant, @boutonApres, @boutonPoubelle, @boutonCrayon, @blocmagique]
            descriptionsBoutonsOutils = ["Ce bouton \"Bombe\" permet d'effacer toutes\n les hypothèses qui ont été saisies dans la grille.",
                            "Ce bouton \"Gomme\"permet d'effacer les hypothèses\n présentent dans une case avec le clic droit\nIl permet également d'effacer la réponse en utilisant le clic gauche.",
                            "Ce bouton \"Undo\" permet de revenir un coup en arrière.",
                            "Ce bouton \"Redo\" permet de revenir un coup en avant.",
                            "Ce bouton \"Poubelle\" permet d'effacer toutes les réponses de la grille.",
                            "Ce bouton \"Crayon\" permet de colorier une case en vert avec le clic droit\n et en jaune avec le clic gauche.",
                            "Ce bouton \"Bloc magique\" permet d'afficher une image \nde l'ensemble des combinaisons appartenant au bloc magique."]

            boutonsAides  = [@Afficher_bloc, @Afficher_pair, @Afficherintersection, @boutonDepassementSomme,@boutonVerifierGrille]
            labelsAides   = ["Afficher les blocs magiques", "Appliquer la règle des paires, triplets et quadruplets ...", "Appliquer la règle des intersections des cases sommes", "Afficher les dépassements de somme","Vérifie la grille"]
            descriptionsBoutonsAides = ["Afficher les cases formant un bloc magique. (une seule combinaison pour les résoudre).\nA chaque clic un nouveau bloc magique est proposé.",
                                  "Enlève des hypothèses les suggestions inutiles en appliquant la règles de paires,\n triplets et quadruplets.Si une case somme possède deux cases différents\n ayant des hypothèses de plus de 2 chiffres similaires,\nalors on efface des autres cases réponses ces chiffres.",
                                  "Permet de réaliser l'intersection entre deux cases somme en \naffichant les hypothèses qui sont en communs.",
                                  "Permet d'afficher les dépassements de somme \nliés aux hypothèses saisies dans les cases \nréponses associés à une case somme donnée.\nCliquer sur le bouton permet d'activer l'aide. \nEnsuite, il faut utiliser le clic droit et le clic \ngauche pour détecter les dépassements de somme.",
                                  "Vérification de la grille en affichant les erreurs et les bonnes réponses."]

            boutonsAutres = [@boutonMenu, @boutonPause, @lblChronometre, @infosJeu]
            labelsAutres  = ["Bouton menu", "Bouton pause chronometre", "Chronometre", "Informations relatives au jeu"]
            descriptionsAutres = ["Ce bouton \"Menu\" permet d'avoir accès au menu.\nA travers ce menu on peut quitter la partie\n ou bien modifier les options du jeu.",
                                "Ce bouton \"Pause\" permet de mettre en pause le chronomètre.",
                                "Ceci est  le \"Chronomètre\".",
                                "Ce sont les informations relatives à la grille."]

            for i in 0..6

                label = Gtk::Label.new(labelsBoutonsOutils[i])
                popover = creerPopover(boutonsOutils[i], label, :bottom)
                popover.show
                @bloctxt.set_label(descriptionsBoutonsOutils[i])
                blocMagique = false
                #Clignotement
                for j in 0..4
                    ancienName = boutonsOutils[i].name
                    boutonsOutils[i].set_name("tuto")
                    sleep(tempo)
                    case i
                    when 0
                        @grilleCasesGlade[2][1].set_label("")
                        @grilleCasesGlade[2][1].set_name("tutoCases")
                        @grilleCasesGlade[2][2].set_label("")
                        @grilleCasesGlade[2][2].set_name("tutoCases")
                        @grilleCasesGlade[2][4].set_label("")
                        @grilleCasesGlade[2][4].set_name("tutoCases")
                    when 1
                        @grilleCasesGlade[2][3].set_label("")
                        @grilleCasesGlade[2][3].set_name("tutoCases")
                    when 2
                        @grilleCasesGlade[2][3].set_label("6")
                        @grilleCasesGlade[2][3].set_name("tutoCases")
                    when 3
                        @grilleCasesGlade[2][3].set_label("")
                        @grilleCasesGlade[2][3].set_name("tutoCases")
                    when 4     
                        for j in 1..4
                            @grilleCasesGlade[3][j].set_label("")
                            @grilleCasesGlade[3][j].set_name("tutoCases")
                        end
                        @grilleCasesGlade[4][3].set_label("")
                        @grilleCasesGlade[4][3].set_name("tutoCases")
                        @grilleCasesGlade[4][4].set_label("")
                        @grilleCasesGlade[4][4].set_name("tutoCases")
                    when 5
                        @grilleCasesGlade[3][4].set_name("green_case")
                        @grilleCasesGlade[2][2].set_name("yellow_case")
                    when 6
                        # if(!blocMagique)
                        #     @bloc.show()
                        #     blocMagique = true   
                        # end                     
                    end
                    boutonsOutils[i].set_name(ancienName)
                    sleep(tempo)
                    @barreDeProgression.fraction += (1/16.0)/5
                end
                # if (blocMagique) then
                #     @bloc.hide()
                #     blocMagique = false
                # end
                for i in 0..@grilleJeu.taille-1
                    for j in 0..@grilleJeu.taille-1
                        if (@grilleJeu.getCase(i,j).class() == CaseReponse && @grilleJeu.getCase(i,j).existeHypotheses())
                            @grilleCasesGlade[i][j].set_name("white_case_hyp")
                        elsif (@grilleJeu.getCase(i,j).class() == CaseReponse )
                            @grilleCasesGlade[i][j].set_name("white_case_rep")
                        end
                    end
                end
                popover.visible = false
                @bloctxt.set_label("")

            end

            #Aides
            @grilleJeu.getCase(2,1).hypotheses = [false, false, false, false, false, false, false, true, true]
            @grilleCasesGlade[2][1].set_label(affichageHypothese(@grilleJeu.getCase(2,1)))
            @grilleCasesGlade[2][1].set_name("white_case_hyp")


            @grilleJeu.getCase(2,2).hypotheses = [false, true, true, false, false, true, true, true, true]
            @grilleCasesGlade[2][2].set_label(affichageHypothese(@grilleJeu.getCase(2,2)))
            @grilleCasesGlade[2][2].set_name("white_case_hyp")

            @grilleCasesGlade[2][3].set_label("6")

            @grilleJeu.getCase(2,4).hypotheses = [false, false, false, false, false, false, false, true, true]
            @grilleCasesGlade[2][4].set_label(affichageHypothese(@grilleJeu.getCase(2,4)))
            @grilleCasesGlade[2][4].set_name("white_case_hyp")

            @grilleCasesGlade[3][1].set_label("1")         
            @grilleCasesGlade[3][2].set_label("4")

            for i in 0..4
                label = Gtk::Label.new(labelsAides[i])
                popover = creerPopover(boutonsAides[i], label, :bottom)
                popover.show
                @bloctxt.set_label(descriptionsBoutonsAides[i])
                for j in 0..4
                    ancienName = boutonsAides[i].name
                    boutonsAides[i].set_name("tuto2")
                    sleep(tempo)
                    case i
                    when 0
                        @grilleCasesGlade[1][1].set_name("bloc_magic")
                        @grilleCasesGlade[1][2].set_name("bloc_magic")
                    when 1
                        @grilleJeu.getCase(2,2).hypotheses = [false, true, true, false, false, true, true, false, false]
                        @grilleCasesGlade[2][2].set_label(affichageHypothese(@grilleJeu.getCase(2,2)))
                        @grilleCasesGlade[2][2].set_name("tutoHypotheses")

                        @grilleCasesGlade[2][1].set_name("pair_case")
                        @grilleCasesGlade[2][2].set_name("pair_case")
                        @grilleCasesGlade[2][4].set_name("pair_case")
                    when 2
                        @grilleJeu.getCase(4,3).hypotheses = [true, true, true, true, true, false, false, false, false]
                        @grilleCasesGlade[4][3].set_label(affichageHypothese(@grilleJeu.getCase(4,3)))
                        @grilleCasesGlade[4][3].set_name("intersection_case")
                    when 3
                    when 4
                        @grilleCasesGlade[2][3].set_name("green_case")
                        @grilleCasesGlade[3][1].set_name("red_case")
                        @grilleCasesGlade[3][2].set_name("red_case")
                    end
                    boutonsAides[i].set_name(ancienName)
                    sleep(tempo)
                    @barreDeProgression.fraction += (1/16.0)/5
                end
                for i in 0..@grilleJeu.taille-1
                    for j in 0..@grilleJeu.taille-1
                        if (@grilleJeu.getCase(i,j).class() == CaseReponse && @grilleJeu.getCase(i,j).existeHypotheses())
                            @grilleCasesGlade[i][j].set_name("white_case_hyp")
                        elsif (@grilleJeu.getCase(i,j).class() == CaseReponse )
                            @grilleCasesGlade[i][j].set_name("white_case_rep")
                        end
                    end
                end
                popover.visible = false
                @bloctxt.set_label("")
            end
            
            # Autres
            chronoPause = false
            for i in 0..3
                label = Gtk::Label.new(labelsAutres[i])
                popover = creerPopover(boutonsAutres[i], label, :bottom)
                popover.show
                @bloctxt.set_label(descriptionsAutres[i])
                for j in 0..4
                    ancienName = boutonsAutres[i].name
                    boutonsAutres[i].set_name("tuto3")
                    sleep(tempo)
                    case i
                    when 1
                        @chronometre.pause()
                        chronoPause = true
                    end
                    boutonsAutres[i].set_name(ancienName)
                    sleep(tempo)
                    @barreDeProgression.fraction += (1/16.0)/5
                end
                if chronoPause then
                    @chronometre.reprendre()
                end
                popover.visible = false
                @bloctxt.set_label("")
            end
            sleep(1)
            cacher
            @MenuPrincipale.afficher
            @chronometre.thread.exit()
            Thread.exit()
        }.run()
    
    end

    ##
    #   Permet d'afficher la page
    #   @param mode le mode de jeu
    #   @param difficulte la difficulte
    def afficher(mode, difficulte)
        super()
        @info = Info.creer(@infosLangue["Partie"][mode.to_s], @infosLangue["Partie"][difficulte.to_s], @etiquetteValeurMode, @etiquetteValeurDifficulte, @etiquetteValeurTentative)
        if(@info.mode == "Tutoriel" || @info.mode == "Tutorial")
            @tutoActif = true
        else
            @tutoActif = false
        end
        if(@chronometre == nil || @chronometre.thread.status == false)
            creerChronometre
        else
            if(@chronometre.pause)
                @chronometre.reprendre()
            end
        end

        @grilleJeu = @profil.grille()
        @chronometre.heures =  @profil.grille.tempsChrono[0]
        @chronometre.minutes =  @profil.grille.tempsChrono[1]
        @chronometre.secondes =  @profil.grille.tempsChrono[2]

        initialiserBoutonsCasesGlade()
        @PaveNumerique.lierPartie(self, (@info.mode == "perfection"))
        @bloctxt.set_label("")

        if(@taille == 5)
            @barreDeProgression.visible = false
        end
        if(@tutoActif)
            tutoriel()
        end  
    end

    ##
    #   Permet de changer de langue sur la page partie
    #   @param infos les informations du JSON
    def swapLangue(infos)
        @infosLangue = infos

        @etiquetteMode.set_text(infos["Partie"]["Mode"])
        @etiquetteTentatives.set_text(infos["Partie"]["Tentatives"])
        @etiquetteDifficulte.set_text(infos["Partie"]["Difficulte"])
        @Afficher_bloc.set_label(infos["Partie"]["Afficher un bloc magique"])
        @Afficher_pair.set_label(infos["Partie"]["Afficher paires"])
        @Afficherintersection.set_label(infos["Partie"]["Afficher intersection"])
        @boutonDepassementSomme.set_label(infos["Partie"]["Afficher depassement"])
        @boutonNouvellePartie.set_label(infos["Partie"]["Nouvelle Grille"])
        @boutonVerifierGrille.set_label(infos["Partie"]["Verifier Grille"])
    end

    ##
    #   Permet d'afficher les hypothèses
    #   @param c la case sur lequel afficher les hypotheses
    def affichageHypothese(c)
        if (c.class() == CaseReponse)
            chaine = String.new()
            if (c.existeHypothese?(1))
                chaine += "1"
            else
                chaine += " "
            end
            chaine += "  "
            if (c.existeHypothese?(2))
                chaine += "2"
            else
                chaine += " "
            end
            chaine += "  "
            if (c.existeHypothese?(3))
                chaine += "3"
            else
                chaine += " "
            end
            chaine += "\n"
            if (c.existeHypothese?(4))
                chaine += "4"
            else
                chaine += " "
            end
            chaine += "  "
            if (c.existeHypothese?(5))
                chaine += "5"
            else
                chaine += " "
            end
            chaine += "  "
            if (c.existeHypothese?(6))
                chaine += "6"
            else
                chaine += " "
            end
            chaine += "\n"
            if (c.existeHypothese?(7))
                chaine += "7"
            else    
                chaine += " "
            end 
                chaine += "  "
            if (c.existeHypothese?(8))
                chaine += "8"
            else
                chaine += " "
            end
            chaine += "  "
            if (c.existeHypothese?(9))
                chaine += "9"
            else
                chaine += " "
            end
        end
    end
end
