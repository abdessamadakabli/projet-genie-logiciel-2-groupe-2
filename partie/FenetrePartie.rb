# encoding: UTF-8
##
#   La partie qui charge l'interface graphique depuis glade a été réalisée par 
#	Pierre Jacoboni le Mercredi 19 décembre à 15:38:33 en 2018
require 'gtk3'
load '../chronometre/Chronometre.rb'
load 'PaveNumeriqueHypothese.rb'
load 'PaveNumeriqueReponse.rb'
load '../menuPrincipal/MenuPrincipale.rb'
load '../options/Option.rb'
load '../nouvellePartie/NouvellePartie.rb'
load '../nouvellePartie/FenetreSelectionGrilles.rb'
load '../aPropos/Apropos.rb'
load '../profil/Profil.rb'
load 'Partie.rb'
load '../nouvellePartie/Partie5.rb'
load '../nouvellePartie/Partie7.rb'
load '../nouvellePartie/Partie10.rb'
load '../nouvellePartie/Pause.rb'
<<<<<<< HEAD
load '../sauvegarde/Sauvegarde.rb'
=======
load '../sauvegarde/sauvegarde.rb'
load '../classement/Classement.rb'
>>>>>>> 9a1af7dd663a49c976efb042ff94afc45c9ea989

# ??? load '../Partie/Sauvegarde.rb'

##
#	Cette classe permet d'afficher la fenêtre qui contiendra la grille de kakuro 
#	ainsi que l'ensemble des fonctionnalités qui y sont liées
class Builder < Gtk::Builder
	##
	#	Ce conctructeur permet de charger l'interface graphique réalisée avec Glade
	def initialize()
	    super()
		self.add_from_file("../glade/FenetrePartie.glade")
		
		# Creation d'une variable d'instance par composant identifié dans glade
		puts "Création des variables d'instances"
	
		@MenuPrincipale = MenuPrincipale.creer
		@Apropos = Apropos.creer(@MenuPrincipale)
		@Classement = Classement.creer(@MenuPrincipale)
		@Profil = Profil.creer(@MenuPrincipale)
		@PaveNumerique = PaveNumeriqueReponse.creer
		@PaveHypothese = PaveNumeriqueHypothese.creer
		@Pause = Pause.creer(@Profil, @MenuPrincipale)
		@Sauvegarde = Sauvegarde.creer(@Profil, @MenuPrincipale)
		@Partie5 = Partie.creer(@Profil, @PaveHypothese, @PaveNumerique, @Pause, 5)
		@Partie7 = Partie.creer(@Profil, @PaveHypothese, @PaveNumerique, @Pause, 7)
		@Partie10 = Partie.creer(@Profil, @PaveHypothese, @PaveNumerique, @Pause, 10)
		#@Partie5 = Partie5.creer(@Profil, @PaveHypothese, @PaveNumerique, @Pause)
		#@Partie7 = Partie7.creer(@Profil, @PaveHypothese, @PaveNumerique, @Pause)
		#@Partie10 = Partie10.creer(@Profil, @PaveHypothese, @PaveNumerique, @Pause)
		@NouvellePartie = NouvellePartie.creer(@MenuPrincipale, @Partie5)
		@FenetreSelectionGrilles = FenetreSelectionGrilles.creer(@Profil, @NouvellePartie, @Partie5, @Partie7, @Partie10)
		@Option = Option.creer(@MenuPrincipale, @Profil, [@MenuPrincipale, @NouvellePartie, @Apropos, @Profil, @Partie5, @Partie7, @Partie10, @Pause, @Sauvegarde, @FenetreSelectionGrilles])

		@listePage = [@NouvellePartie, @MenuPrincipale, @Apropos, @Profil, @Pause, @Sauvegarde, @Partie5, @Partie7, @Partie10, @FenetreSelectionGrilles, @Option, @Classement]

		self.objects.each() { |p| 	
				unless p.builder_name.start_with?("___object")
					puts "\tCreation de la variable d'instance @#{p.builder_name}"
					if p.builder_name.start_with?("PRINCIPAL") then
						@MenuPrincipale.ajouterVariable(p.builder_name.delete_prefix("PRINCIPAL_"), self[p.builder_name])
					elsif p.builder_name.start_with?("NOUVELLE") then
						@NouvellePartie.ajouterVariable(p.builder_name.delete_prefix("NOUVELLE_"), self[p.builder_name])
					elsif p.builder_name.start_with?("OPTIONS") then
						@Option.ajouterVariable(p.builder_name.delete_prefix("OPTIONS_"), self[p.builder_name])
					elsif p.builder_name.start_with?("APROPOS") then
						@Apropos.ajouterVariable(p.builder_name.delete_prefix("APROPOS_"), self[p.builder_name])
					elsif p.builder_name.start_with?("CHARGER") then
						@Sauvegarde.ajouterVariable(p.builder_name.delete_prefix("CHARGER_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PROFIL") then
						@Profil.ajouterVariable(p.builder_name.delete_prefix("PROFIL_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PARTIE5") then
						@Partie5.ajouterVariable(p.builder_name.delete_prefix("PARTIE5_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PARTIE7") then
						@Partie7.ajouterVariable(p.builder_name.delete_prefix("PARTIE7_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PARTIE10") then
						@Partie10.ajouterVariable(p.builder_name.delete_prefix("PARTIE10_"), self[p.builder_name])
					elsif p.builder_name.start_with?("CLASSEMENT") then
						@Classement.ajouterVariable(p.builder_name.delete_prefix("CLASSEMENT_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PAVENUM") then
						@PaveNumerique.ajouterVariable(p.builder_name.delete_prefix("PAVENUM_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PAVEHYPO") then
						@PaveHypothese.ajouterVariable(p.builder_name.delete_prefix("PAVEHYPO_"), self[p.builder_name])
					elsif p.builder_name.start_with?("PAUSE") then
						@Pause.ajouterVariable(p.builder_name.delete_prefix("PAUSE_"), self[p.builder_name])
					elsif p.builder_name.start_with?("SELECTION") then
						@FenetreSelectionGrilles.ajouterVariable(p.builder_name.delete_prefix("SELECTION_"), self[p.builder_name])
					end
					instance_variable_set("@#{p.builder_name}".intern, self[p.builder_name]) 
				end
		}

		@MenuPrincipale.ajouterVariable("ChargerPartie", @Sauvegarde)
		@MenuPrincipale.ajouterVariable("NouvellePartie", @NouvellePartie)
		@MenuPrincipale.ajouterVariable("Profil", @Profil)
		@MenuPrincipale.ajouterVariable("Option", @Option)
		@MenuPrincipale.ajouterVariable("Apropos", @Apropos)
		@MenuPrincipale.ajouterVariable("Classement", @Classement)
		@NouvellePartie.ajouterVariable("FenetreSelectionGrilles", @FenetreSelectionGrilles)
		@Profil.ajouterVariable("Option", @Option)
		@Pause.ajouterVariable("Option", @Option)

		@listePage.each do |p| 
			p.ajouterVariable("fenetre", @PRINCIPAL_fenetreMenuPrincipale)
		end
		@PaveNumerique.ajouterVariable("fenetre", @PAVENUM_fenetrePaveNumerique)
		@PaveHypothese.ajouterVariable("fenetre", @PAVEHYPO_fenetrePavehypothese)

		provider = Gtk::CssProvider.new

        provider.load_from_path('../style/couleur.css')
    
    
      	Gtk::StyleContext.add_provider_for_screen(Gdk::Screen.default, provider, Gtk::StyleProvider::PRIORITY_APPLICATION)

		# On connecte les signaux aux méthodes (qui doivent exister)

		@listePage.each do |p|
			p.initialiserSignaux
		end
		@PaveNumerique.initialiserSignaux
		@PaveHypothese.initialiserSignaux

		@Sauvegarde.getToutesLesSauvegardes
		@Option.initOptions
		@Profil.getTousLesProfils

		@PRINCIPAL_fenetreMenuPrincipale.window_position = Gtk::WindowPosition::CENTER_ALWAYS
		@PRINCIPAL_fenetreMenuPrincipale.resizable=(false)
		@PRINCIPAL_fenetreMenuPrincipale.set_title("Kakuro")
		@PRINCIPAL_fenetreMenuPrincipale.set_icon("../jambon.jpg")
		@PRINCIPAL_fenetreMenuPrincipale.signal_connect('destroy') { 
			puts "Au Revoir !!!"; Gtk.main_quit 
		}

		@PRINCIPAL_fenetreMenuPrincipale.show
	end
end
# On lance l'application
builder = Builder.new()
Gtk.main