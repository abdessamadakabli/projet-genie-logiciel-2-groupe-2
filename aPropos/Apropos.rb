##
#   A propos du jeu , qui contient des informatins sur le groupe et son organisation
class Apropos < Page
    ##
    #   Permet de créer un apropos
    def Apropos.creer()
        new()
    end

    ##
    #   Permet d'initialiser le signal présent sur le bouton
    def initialiserSignaux()
        @fenetreApropos.remove(@base)

        @btn_retourApropos.signal_connect('clicked'){
            cacher
            @MenuPrincipale.afficher
        }
    end
    ##
    #   Permet de changer de langue
    #   @param infos Les informations ,à traduire
    def swapLangue(infos)
        @redactionApropos.set_text(infos["A propos"]["Redaction"])
        @btn_retourApropos.set_label(infos["A propos"]["Retour"])
    end
end