require "../cases/Case.rb"
require "../cases/CaseReponse.rb"

##
#   Cette classe définit une case somme de la grille
#   La case somme permet d'indiquer au joueur la somme à effectuer horizontalement
#   et/ou verticalement
class CaseSomme < Case

    attr_reader :sommeHorizontale,:sommeVerticale,:sommeHorizontaleActuelle,:sommeVerticaleActuelle,:nbCasesHorizontales,:nbCasesVerticales
    attr_accessor :isBlocMagicHorizontale,:isBlocMagicVerticale

    ##
    #   Constructeur permettant d'initialiser une case somme avec une couleur 
    #   et mettre à zéro l'ensemble des sommes qu'elle gère
    #   @param grille  Un ensemble de cases formant la grille du jeu
    #   @param couleur La couleur de la case
    #   @param x La ligne sur laquelle se situe la case
    #   @param y La colonne sur laquelle se situe la case
    #   @param sommeHorizontale La somme horizontale à faire
    #   @param nbCasesHorizontales Le nombre de cases associées à la somme horizontale (nil s'il n'y a pas de cases)
    #   @param sommeVerticale La somme verticale à faire
    #   @param nbCasesVerticales Le nombre de cases associées à la somme verticale (nil s'il n'y a pas de cases)
    def initialize(grille, couleur, x, y, sommeHorizontale, nbCasesHorizontales, sommeVerticale, nbCasesVerticales)
        super(grille,couleur, x, y)
        @sommeHorizontale, @sommeVerticale = sommeHorizontale, sommeVerticale
        @sommeHorizontaleActuelle, @sommeVerticaleActuelle = 0,0
        @nbCasesHorizontales, @nbCasesVerticales = nbCasesHorizontales,nbCasesVerticales
        @isBlocMagicHorizontale = false
        @isBlocMagicVerticale = false
        @interseList = []
    end

    ##
    #   New étant privée, on utilise cette méthode pour créer un objet Case
    #   @param grille  Un ensemble de cases formant la grille du jeu
    #   @param couleur  La couleur de la case 
    #   @param x La ligne sur laquelle se situe la case
    #   @param y La colonne sur laquelle se situe la case
    #   @param sommeHorizontale La somme horizontale à faire
    #   @param nbCasesHorizontales Le nombre de cases associées à la somme horizontale (nil s'il n'y a pas de cases)
    #   @param sommeVerticale La somme verticale à faire
    #   @param nbCasesVerticales Le nombre de cases associées à la somme verticale (nil s'il n'y a pas de cases)
    def CaseSomme.creer(grille, couleur, x, y, sommeHorizontale, nbCasesHorizontales, sommeVerticale, nbCasesVerticales)
        new(grille,couleur, x, y, sommeHorizontale, nbCasesHorizontales, sommeVerticale, nbCasesVerticales)
    end

    ##
    #   Calculer la somme actuelle réalisée grâce aux cases horizontales
    def calculerSommeActuelleHorizontale()
        @sommeHorizontaleActuelle = 0
        for y in @y+1..@y+@nbCasesHorizontales
            if(@grille[@x][y].existeReponse())
                @sommeHorizontaleActuelle += @grille[@x][y].reponse
            end
        end
        return @sommeHorizontaleActuelle
    end

    ##
    #   Calculer la somme actuelle réalisée grâce aux cases verticales
    def calculerSommeActuelleVerticale()
        @sommeVerticaleActuelle = 0
        for x in @x+1..@x+@nbCasesVerticales
            if(@grille[x][@y].class == CaseReponse)
                if(@grille[x][@y].existeReponse())
                    @sommeVerticaleActuelle += @grille[x][@y].reponse
                end
            end
        end
        return @sommeVerticaleActuelle
    end

    ##
    #   Permet de vérifier si la case somme est associée à une somme verticale
    #   @return [Boolean] Vrai s'il existe une somme verticale, faux sinon
    def ExisteSommeVerticale?()
        return  nbCasesVerticales != nil    
    end

    ##
    #   Vérifier si la case somme est associée à une somme horizontale
    #   @return [Boolean] Vrai s'il existe une somme horizontale, faux sinon
    def ExisteSommeHorizontale?()
        return  nbCasesHorizontales != nil    
    end

    ##
    #   Verifier si dans une somme horizontale il existe plusieurs fois un même nombre
    #   @return [Integer] Indice du doublon, -1 sinon
    def existeDoublonsHorizontale?()
        hVerif = new Hash()
        # Quantifier la présence des nombres sur la ligne
        verification = new Array(0)
        for y in 0..@nbCasesHorizontales
            if(@grille[@x][y].reponse != nil)
                hVerif[@grille[@x][y].reponse] += 1
            end
        end
        # Vérifier la présence multiple d'un même nombre
        verification.select.with_index{ |valeur, index|
            if(valeur > 2)
                return index
            end
        }
        return -1
    end


    ##
    #   Verifier si dans une somme verticale il existe plusieurs fois un même nombre
    #   @return [Integer] Indice du doublon, -1 sinon
    def existeDoublonsVerticale?()
        hVerif = new Hash()
        # Quantifier la présence des nombres sur la colonne
        verification = new Array(0)
        for x in 0..@nbCasesVerticales
            if(@grille[x][@y].reponse != nil)
                hVerif[@grille[x][@y].reponse] += 1
            end
        end
        # Vérifier la présence multiple d'un même nombre
        verification.select.with_index{ |valeur, index|
            if(valeur > 2)
                return index
            end
        }
        return -1
    end

    ##
    #   Permet de décomposer un nombre en somme de 'nbCases' nombres différents
    #   @param somme la somme à atteindre
    #   @param nbCases Le nombre de cases nécéssaires pour atteindre la somme
    def combinaisonsSomme(somme, nbCases)
        nombres = (1..9).to_a
        return combinaisons = nombres.combination(nbCases).to_a.select { |res| somme == res.sum }
    end

    ##
    #   Déterminer les combinaisons possibles pour atteindre sommeHorizontale en nbCasesHorizontales
    #   @return [Array] Un tableau contenant des tableaux symbolisant les différentes 
    #   combinaisons possibles pour réaliser une somme horizontale 
    def combinaisonPossiblesSommeHorizontale()
        combinaisons = combinaisonsSomme(@sommeHorizontale, @nbCasesHorizontales)
        reponses = []
        res =[]
        for y in @y+1..@y+@nbCasesHorizontales
            if(@grille[@x][y].existeReponse())
                reponses.push(@grille[@x][y].reponse)
            end
        end
        index = 0
        for comb in combinaisons
            if(reponses != [])
                if((reponses - comb).empty?)
                    res.push(comb)
                end
            else
                res.push(comb)
            end
            index +=1
        end
        return res
    end

    ##
    #   Déterminer les combinaisons possibles pour atteindre sommeVerticale en nbCasesHorizontales
    #   @return [Array] Un tableau contenant des tableaux symbolisant les différentes 
    #   combinaisons possibles pour réaliser une somme verticale
    def combinaisonPossiblesSommeVerticale()
        combinaisons =  combinaisonsSomme(@sommeVerticale, @nbCasesVerticales)
        reponses = []
        res =[]
        for x in @x+1..@x+@nbCasesVerticales
            if(@grille[x][@y].class == CaseReponse)
                if(@grille[x][@y].existeReponse())
                    reponses.push(@grille[x][@y].reponse)
                end
            end
        end
        index = 0
        for comb in combinaisons
            if((reponses - comb).empty?)
                res.push(comb)
            end
            index +=1
        end
        return res
    end

    ##
    #   Permet de réaliser l'affiche des combinaisons
    #   @param combinaisons Les combinaisons à afficher
    #   @param sens Verticale/Horizontale
    def combinaisonsAffichage(combinaisons, sens)
        if(sens == "v")
            somme = @sommeVerticale
        else
            somme = @sommeHorizontale
        end
        combinaisonsAffichage = "Combinaisons de somme possibles pour #{somme}\n"
        index = 0
        indicateur = true
        for comb in combinaisons
            for chiffre in comb
                indicateur = false
                if(index != comb.size-1)
                    combinaisonsAffichage +=  "#{chiffre} + "
                else
                    combinaisonsAffichage += "#{chiffre}"
                end
                index +=1
            end
            combinaisonsAffichage += "\n"
            index = 0
        end
        if(indicateur)
            return ""
        end
        return combinaisonsAffichage
    end

    ##
    #   Permet de savoir s'il existe une seule combinaison de nbCases qui totalise la somme horizontale.
    #   @return [Boolean] Vrai s'il y a une seule combinaison, faux sinon
    def estBlocMagiqueSommeHorizontale?()
        combi = combinaisonPossiblesSommeHorizontale()
        return combi.length == 1 && combi[0].empty? == false
    end

    ##
    #   Permet de savoir s'il existe une seule combinaison de nbCases qui totalise la somme Verticale.
    #   @return [Boolean] Vrai s'il y a une seule combinaison, faux sinon
    def estBlocMagiqueSommeVerticale?()
        combi = combinaisonPossiblesSommeVerticale()
        return combi.length == 1 && combi[0].empty? == false
    end

    ##
    #   Permet de réaliser le traitement indépendant du calcule d'occurences ("paires/triplets/quadruplet etc")
    #   @param hypotheses Un tableau contenant les hypothèses de chaque case apprtement à la case somme sélectionnée
    #   @param [nbOccurences Le nombre d'occurences qu'on souhaite détécter (paires, triplets, ...)
    #   @return [Array] Renvoie le tableau des hypothèses modifiés
    def traitementOccurences(hypotheses, nbOccurences)
        hypothesesNombres = Array.new()

        hypotheses.each{|h|
            hypotheseTableau = Array.new()
            for i in 0..8
                if (h[i] == true)
                    hypotheseTableau.push(i+1)
                end
            end
            hypothesesNombres.push(hypotheseTableau)
        }
        
        #Détecter les paires/triplets ...
        occurences = Hash.new 0
        hypothesesNombres.each do |t|
            occurences[t] += 1
        end
        #Récupérer les indexs des paires triplets ...
        indexsOccurrences = [] 
        occuranceFinale = []
        occurences.select{|occu|
            if(occurences[occu] == nbOccurences)
                indexsOccurrences = hypothesesNombres.each_index.select{|i| hypothesesNombres[i] == occu}
                occuranceFinale = occu
            end
        }
        #On supprime les nombres appartenant aux paires/triplets etc des autres hypothèses
        index = 0
        hypothesesNombres.each{|h|
            if(h - occuranceFinale != [])
                hypothesesNombres[index] = h-occuranceFinale
            end
            index += 1
        }

        hypothesesBoolean = Array.new()
        hypothesesNombres.each{|h|
            hypotheseTableau = Array.new()
            indexFalse = 0
            indexTrue = 0
            index = 0
            while(index < 9)
                if((index+1) == h[indexTrue])
                    hypotheseTableau[index] = true
                    indexTrue+=1
                else
                    hypotheseTableau[index] = false
                end
                index += 1
            end

            hypothesesBoolean.push(hypotheseTableau)
        }

        return hypothesesBoolean
    end

    ##
    #   Permet de rassembler l'ensemble des tableaux d'hypothèses apmpartenant à la somme verticale dans un  tableau
    #   @return Le tableau contenant l'ensemble des tableaux d'hypothèses
    def tableauHypothesesVerticales()
        hypotheses = Array.new()
        for x in @x+1..@x+@nbCasesVerticales
            hypotheses.push(@grille[x][@y].hypotheses)
        end
        return hypotheses
    end

    ##
    #   Permet de rassembler l'ensemble des tableaux d'hypothèses apmpartenant à la somme horizontale dans un tableau
    #   @return Le tableau contenant l'ensemble des tableaux d'hypothèses
    def tableauHypothesesHorizontales()
        hypotheses = Array.new()
        for y in @y+1..@y+@nbCasesHorizontales
            hypotheses.push(@grille[@x][y].hypotheses)
        end
        return hypotheses
    end

    ##
    #   Permet de détecter la présence de paires, triplets ou quadruplets dans les hypothèses 
    #   des cases relatives à la case somme verticale
    #   @param nbOccurences Correspond à la paire, triplets, quadruplets etc...
    def occurrencesVerticales(nbOccurences)
        #   On regroupe l'ensemble des tableaux d'hypothèses dans un seul tableau
        hypotheses = tableauHypothesesVerticales()
        hypotheses = traitementOccurences(hypotheses, nbOccurences)
        #   On actualise le tableau des hypothèses de chaque case
        index = 0
        for x in @x+1..@x+@nbCasesVerticales
            @grille[x][@y].hypotheses = hypotheses[index]
            index +=1
        end
    end
    ##
    #   Permet de détecter la présence de paires, triplets ou quadruplets dans les hypothèses 
    #   des cases relatives à la case somme horizontale
    #   @param [Integer] nbOccurences Correspond à la paire, triplets, quadruplets etc...
    def occurrencesHorizontales(nbOccurences)
        #   On regroupe l'ensemble des tableaux d'hypothèses dans un seul tableau
        hypotheses = tableauHypothesesHorizontales()
        hypotheses = traitementOccurences(hypotheses, nbOccurences)
        #   On actualise le tableau des hypothèses de chaque case
        index = 0
        for y in @y+1..@y+@nbCasesHorizontales
            @grille[@x][y].hypotheses = hypotheses[index]
            index +=1
        end
    end

    ##
    #   Permet de générer le produit cartésien d'un ensemble de tableaux 
    #   et d'en sélectionner ceux dont la somme dépasse une somme donnée
    #   @param hypotheses Le tableau d'hypothèses
    #   @param somme La somme à ne pas dépasser
    #   @return Un tableau contenant l'ensemble des combinaisons qui dépasse la somme donnée
    def traitementDepassementSommeHypotheses(hypotheses, somme)
        resultats = []
        hypothesesNombres = Array.new()
        hypotheses.each{|h|
            hypotheseTableau = Array.new()
            for i in 0..8
                if (h[i] == true)
                    hypotheseTableau.push(i+1)
                end
            end
            hypothesesNombres.push(hypotheseTableau)
        }
        combinaisonSomme = hypothesesNombres[0].product(*hypothesesNombres[1..-1])

        combinaisonSomme.each{|uneCombinaison|
            if(uneCombinaison.inject(:+) > somme)
                if(uneCombinaison.uniq.size == uneCombinaison.size)
                    resultats.push(uneCombinaison)
                end
            end
        }  

        return resultats
    end

    ##
    #   Permet de détecter les dépassements de somme dans les hypothèses verticales
    def detecterDepassementSommeVerticale()
        hypotheses = tableauHypothesesVerticales()
        resultats  = traitementDepassementSommeHypotheses(hypotheses, @sommeVerticale)
        return resultats
    end

    ##
    #   Permet de détecter les dépassements de somme dans les hypothèses horizontales
    def detecterDepassementSommeHorizontale()
        hypotheses = tableauHypothesesHorizontales()
        resultats  = traitementDepassementSommeHypotheses(hypotheses, @sommeHorizontale)
        return resultats
    end
    ##
    #   Permet d'afficher les dépassements de somme
    #   @param resultats les combinaisons d'hypothèses qui dépassement la somme
    #   @param sens Verticale/Horizontale
    def afficherDepassementSomme(resultats, sens)
        affichage = ""
        nbDepassements = 0
        nbDepassementsAffiche = 0
        indicateur = true
        somme = 0
        combinaisons = []
        index = 0

        if(sens == "v")
            somme = @sommeVerticale
            nbCases = @nbCasesVerticales
        else
            somme = @sommeHorizontale
            nbCases = @nbCasesHorizontales
        end
        for res in resultats
            if(res != [])
                combinaisons.push(res)
                nbDepassements +=1
            end 
        end
        
        if(nbDepassements > 7)
            nbDepassementsAffiche = rand(4) + 3
        else
            nbDepassementsAffiche = nbDepassements-1
        end
        coord = "#{@x+1},#{(@y.ord+'A'.ord).chr}"
        affichage = "- Case somme (#{coord})\n- Il existe #{nbDepassements} combinaison(s) qui dépasse la somme attendue\n- En voici #{nbDepassementsAffiche} affichées\n"
        cpt = 0 
        for comb in combinaisons
            if(cpt == nbDepassementsAffiche)
                break
            end
            for chiffre in  comb
                indicateur = false
                if(index != comb.size-1)
                    affichage +=  "#{chiffre} + "
                else
                    affichage += "#{chiffre}"
                end
                index +=1
            end
            index = 0
            affichage += "> #{somme}\n"
            cpt += 1
        end
        if(indicateur)
            return "Aucun dépassement de somme détécté."
        end
        return affichage
    end

    ##
    #   Permet de determiner l'ensemble des intersections des cases de cette case somme avec les cases d'une autre case somme
    def determinerIntersections()
        casesSomme = Array.new

        #On récupère les cases somme       
        for cases in @grille
            for uneCase in cases
                #   vérifier == / ===
                if uneCase.class() == CaseSomme && !(uneCase == self)
                    casesSomme.push(uneCase)
                end
            end
        end  

        #On détermine pour chaque case les cases qui sont en intersection avec elle
        lTest = []
        hasInte = true
        hori = false
        while (lTest.length != casesSomme.length && hasInte) do
            r = rand(0..(casesSomme.length-1))
            if (!lTest.include?(casesSomme[r])) then
                lTest << casesSomme[r]
                if (horizontalConnexion(casesSomme[r]) || verticalConnexion(casesSomme[r])) then
                    if (horizontalConnexion(casesSomme[r])) then
                        hori = true
                    end
                    hasInte = hasIntesecWith?(casesSomme[r])
                end
            end
        end

        if (hasInte) then
            return nil
        end

        c = casesSomme[r]
        @interseList << c
        c.addIntesecWith(self)
        
        x = 0
        y = 0

        if (hori) then
            listeAND = listAndlist(combinaisonPossiblesSommeHorizontale(), c.combinaisonPossiblesSommeVerticale())
            x = @x
            y = c.getY
        else
            listeAND = listAndlist(combinaisonPossiblesSommeVerticale(), c.combinaisonPossiblesSommeHorizontale())
            y = @y
            x = c.getX
        end

        c = @grille[x][y]
        if(!listeAND.empty?)
            listeAND.each { |i|
                @grille[x][y].saisirHypotheses(i)
            }
            return @grille[x][y]
        else
            return nil
        end
    
    end

    ##
    #   Permet de faire un union entre deux ligne
    #   @param l1 La liste des hypothèses n°1
    #   @param l2 La liste des hypothèses n°2
    def listAndlist(l1, l2)
        land = []
        l1.each { |i|
            i.each { |k|
                l2.each { |j|
                    if j.include?(k) && !land.include?(k) then
                        land << k
                    end
                }
            }    
        }
        return land
    end

    ##
    #   Permet de savoir si la case rentrer en parametre est comprise dans la verticalité de cette case
    #   @param c2 la case
    def verticalConnexion(c2)
        if (@nbCasesVerticales+@x) >= c2.getX && @x < c2.getX then
            return true
        end
        return false
    end

    ##
    #   Permet de savoir si la case rentrer en parametre est comprise dans l'horizontalité de cette case
    #   @param c2 la case
    def horizontalConnexion(c2)
        if (@nbCasesHorizontales+@y) >= c2.getY && @y < c2.getY then
            return true
        end
        return false
    end

    ##
    #   Permet d'ajouter une case a la liste des intersections déjà faite
    #   @param c la case
    def addIntesecWith(c)
        @interseList << c
    end

    ##
    #   Permet de savoir si une case à une intersection avec une autre déjà renseigner
    #   @param c la case
    def hasIntesecWith?(c)
        return @interseList.include?(c)
    end 


    ##
    #   Rédéfinition
    #   Permet d'associer un code json de la case réponse
    def json()
        return @grilleJson.jsonCaseSomme(@sommeVerticale, @nbCasesVerticales, @sommeHorizontale, @nbCasesHorizontales)
    end

    ##
    #   Rédéfinition de la méthode to_s pour afficher une case
    #   @return [String] Le rerprésentation sous forme de string d'une case
    def to_s()
        return super.to_s + ", nbCasesVerticales = #{@nbCasesVerticales}, nbCasesHorizontales : #{@nbCasesHorizontales})"
    end

    private_class_method :new

end