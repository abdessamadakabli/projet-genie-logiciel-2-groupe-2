require  "../cases/Case.rb"

##
#   Cette classe définit une case réponse
#   La case réponse peut acceuillir à la fois les hypothèses (1 minimum et 9 maximum) du joueur
#   ou bien une valeur unique 
class CaseReponse < Case
    attr_accessor :reponse,:hypotheses,:couleur,:x,:y
    ##
    #   Constructeur permettant d'initialiser une case réponse
    #   @param grille  Un ensemble de cases formant la grille du jeu
    #   @param couleur  La couleur de la case 
    #   @param x La ligne sur laquelle se situe la case
    #   @param y La colonne sur laquelle se situe la case
    #   @param corrige La colonne sur laquelle se situe la case
    def initialize(grille,couleur, x, y, corrige)
        super(grille,couleur, x, y)
        nbHypotheses = 9
        @reponse    = -1
        @corrige    = corrige
        @hypotheses = Array.new(nbHypotheses, false)
    end

    ##
    #   New étant privée, on utilise cette méthode pour créer un objet CaseReponse
    #   @param grille  Un ensemble de cases formant la grille du jeu
    #   @param couleur  La couleur de la case 
    #   @param x La ligne sur laquelle se situe la case
    #   @param y La colonne sur laquelle se situe la case
    #   @param corrige La bonne réponse
    def CaseReponse.creer(grille, couleur, x, y, corrige)
        new(grille,couleur, x, y, corrige)
    end

    ##
    #   Permet de saisir une réponse dans la case
    #   @param reponse La réponse que l'utilisateur souhaite saisir
    def saisirReponse(reponse)
        @reponse = reponse
    end

    ##
    #   Efface la valeur réponse choisie par l'utilisateur
    def effacerReponse()
        @reponse = -1
    end

    ##
    #   Permet de saisir des hypothèses dans l'interval suivant : [1,9]
    #   @param hypothese L'hypothèse que l'utilisateur souhaite écarter
    def saisirHypotheses(hypothese)
        @hypotheses[hypothese-1] = true
    end
    
    ##
    #   Permet d'effacer l'ensemble des hypothèses saisies par l'utilisateur
    def effacerHypotheses()
        @hypotheses.fill(false)
    end

    ##
    #   Permet d'effacer une hypothèse en particulier
    #   @param hypothese L'hypothèse que l'utilisateur souhaite écarter
    def effacerUneHypothese(hypothese)
        @hypotheses[hypothese-1] =  false
    end

    ##
    #   Permet de vérifier s'il existe des hypothèses dans la caseReponse
    #   @return [Boolean] Vrai s'il existe des hypothèses, faux sinon
    def existeHypotheses()
        return @hypotheses.include?(true)
    end

    ##
    #   Permet de vérifier l'existence d'une hypothèse dans la caseReponse
    #   @param valeur la valeur dont on veut vérifier l'existence
    #   @return  Vrai si elle existe
    def existeHypothese?(valeur)
        return @hypotheses[valeur-1] == true
    end

    ##
    #   Permet de vérifier s'il existe une réponse insérée dans la caseReponse
    #   @return [Boolean] Vrai s'il existe une réponse, faux sinon
    def existeReponse()
        return @reponse != nil && @reponse != -1
    end

    ##
    #   Permet d'afficher la réponse saisie dans la case
    def afficherReponse()
        puts @reponse
    end

    ##
    #   Permet d'afficher les hypothèses saisies dans la case
    def afficherHypotheses()
        puts @hypotheses
    end

    ##
    #   Rédéfinition
    #   Permet d'associer un code json de la case réponse
    def json()
        if(!existeReponse())
            @reponse = -1
        end
        return @grilleJson.jsonCaseReponse(@corrige, @reponse, @hypotheses)
    end

    ##
    #   Permet de vérifier si (pour CaseReponse) la réponse et le corrigé sont les mêmes, destiné à l'héritage
    #   pour déléguer aux cases la vérification
    def verifierReponse()
        return @reponse == @corrige
    end

    ##
    #   Rédéfinition de la méthode to_s pour afficher une case
    #   @return [String] Le rerprésentation sous forme de string d'une case
    def to_s()
        return super.to_s + ", reponse = #{@reponse}), hypotheses = #{@hypotheses}"
    end

    private_class_method:new

end