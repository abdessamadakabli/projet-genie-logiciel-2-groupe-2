require "../cases/Case.rb"

##
#   Cette classe représente une case neutre de la grille Kakuro.
#   Une case neutre est une case qui n'est pas accessible au joueur 
class CaseNeutre < Case
    ##
    #   Cette classe hérite des variables d'instance de sa classe mère Case

    ##
    #   Cette classe est initialisée de la même manière que sa classe mère
    #   Donc il n 'y pas besoin de définir de méthode initialize

    ##
    #   New étant privée, on utilise cette méthode pour créer un objet CaseNeutre
    #   @param grille  Un ensemble de case formant la grille du jeu
    #   @param couleur  La couleur de la case 
    #   @param x La ligne surlaquelle se situe la case
    #   @param y La colonne surlaquelle se situe la case
    def CaseNeutre.creer(grille,couleur, x, y)
        new(grille,couleur, x, y)
    end

    ##
    #   Rédéfinition
    #   Permet d'associer un code json de la case réponse
    def json()
        return @grilleJson.jsonCaseNeutre()
    end

    ##
    #   Rédéfinition de la méthode to_s pour afficher une case
    #   @return [String] Le rerprésentation sous forme de string d'une case
    def to_s()
        return super.to_s + ")"
    end

    private_class_method :new

end
