require '../grille/GrilleJson.rb'

##
#   Cette classe définit une case de la grille Kakuro.
#   Elle regroupe l'ensemble des varaibles d'instance communes
#   aux autres cases composant la grille.
class Case
    attr_accessor :couleur, :suivante
    attr_reader :x, :y
    ##  
    #   Constructeur permettant d'initialiser la case dans la grille et donc sa couleur 
    #   @param grille  Un ensemble de case formant la grille du jeu
    #   @param couleur  La couleur de la case 
    #   @param x La ligne surlaquelle se situe la case
    #   @param y La colonne surlaquelle se situe la case
    def initialize(grille,couleur, x, y)
        @grille = grille
        @couleur  = couleur
        @x, @y = x, y
        @suivante = nil
        @grilleJson = GrilleJson.creer()
    end

    ##
    #   Permet d'obtenir la ligne sur laquelle se trouve une case
    #   @return Integer La ligne sur laquelle se trouve une case
    def getX() 
        return @x
    end

    ##
    #   Permet d'obtenir la colonne sur laquelle se trouve une case
    #   @return Integer La colonne sur laquelle se trouve une case 
    def getY() 
        return @y
    end

    ##
    #   Efface la valeur réponse choisie par l'utilisateur
    def effacerReponse()
    end

    ##
    #   Permet d'effacer l'ensemble des hypothèses saisies par l'utilisateur
    def effacerHypotheses()
    end

    ##
    #   Permet d'associer un code json à une case donnée
    def json()
    end

    ##
    #   Permet de vérifier si (pour CaseReponse) la réponse et le corrigé sont les mêmes, destiné à l'héritage
    #   pour déléguer aux cases la vérification, par défaut retourne vrai
    def verifierReponse()
        return true
    end
    
    ##
    #   Rédéfinition de la méthode to_s pour afficher une case
    #   @return [String] Le rerprésentation sous forme de string d'une case
    def to_s()
        return self.class.name + "(couleur = #{@couleur}, x = #{@x}, y = #{@y}"
    end

    private_class_method :new

end