##
#   Permet de créer une fenêtre caoable de s'afficher ou bien se cacher
class Page
    ##
    #   Permet d'ajouter des variables
    #   @param Le nom de la variable d'instance à créer
    #   @param L'object associé à cette variable d'instance
    def ajouterVariable (nom, objet)
        instance_variable_set("@#{nom}".intern, objet)
    end

    ##   
    #   Permet d'afficher une page
    def afficher()
        @fenetre.add(@base)
    end
    
    ##
    #   Permet de cacher une page
    def cacher()
        @fenetre.remove(@base)
    end

end