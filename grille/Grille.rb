require 'json'
require "../cases/CaseNeutre.rb"
require "../cases/CaseReponse.rb"
require "../cases/CaseSomme.rb"
require "../grille/Historique.rb"

##
#   Représente la grille d'une partie de Kakuro
#   Contient une matrice de cases et un historique de coups
class Grille

    attr_accessor :grille, :fichierGrille, :grilleGlade, :tempsChrono
    attr_reader :taille, :historique


    ##
    #   Méthode de classe redéfinissant new() pour y ajouter un paramètre
    #   @param fichier L'emplacement du fichier contenant la grille
    def Grille.creer(fichier)
        new(fichier)
    end
    private_class_method:new

    ##
    #   Constructeur de la Grille du Kakuro
    #   @param fichier L'emplacement du fichier contenant la grille
    def initialize(fichier)
        @taille = nil
        @grilleJson = GrilleJson.creer()
        @fichierGrille = fichier
        @grille = nil
        @tempsChrono = [0,0,0]
        charger()
        @historique = Historique.creer(self)
        @grilleGlade = nil
    end

    ##
    #   Permet d'obtenir une case
    #   @param i la ligne sur laquelle se trouve la case
    #   @param j la colonne sur laquelle se trouve la case
    def getCase(i, j)
        return @grille[i][j]
    end

    ##
    #   Permet d'établir les cases provenant du glade
    #   @param grilleGlade Array La grille contenant les cases glade
    def setGrilleGlade(grilleGlade)
        @grilleGlade = grilleGlade
    end
    
    ##
    #   Permet d'effacer toutes les réponses inscrites dans la grille
    def effacerReponses()
        for i in 0..@taille-1
            @grille[i].effacerReponse()
        end
    end

    ##
    #   Permet d'effacer toutes les hypothèses inscrites dans la grille
    def effacerHypotheses()
        for i in 0..@taille-1
            @grille[i].effacerHypotheses()
        end
    end

    ##
    #   Permet d'effacer toutes les réponses ET hypothèses inscrites dans la grille
    def effaceToi()
        for i in 0..@taille-1
            @grille[i].effacerReponse()
            @grille[i].effacerHypotheses()
        end
    end
    
    ##
    #   Permet d'effacer les hypothèses présentes sur la grille
    #   @param indiceLigne Integer L'indice d'une ligne sur les cases glades
    def effacerLigneHypotheses(indiceLigne)
        @grille[indiceLigne].each_with_index{ |c, index|
            if (getCase(indiceLigne,index).class() == CaseReponse)
                if (c.existeHypotheses())
                    @grilleGlade[indiceLigne][index].set_label("")
                    c.effacerHypotheses()
                end
            end
        }
    end

    ##
    #   Permet d'effacer l'ensemble des hypothèses de la grille
    def effacerToutesLesHypotheses()
        for indiceLigne in 0..4
            effacerLigneHypotheses(indiceLigne)
        end
        @grilleGlade[0][0].set_label("\n\n                ")
    end

    ##
    #   Effacer une ligne contenant des réponses dans les cases réponse   
    #   @param indiceLigne l'indice de la ligne en question
    def effacerLigneReponse(indiceLigne)
        @grille[indiceLigne].each_with_index{ |c, index|
            if (getCase(indiceLigne,index).class() == CaseReponse)
                if (c.existeReponse())
                    @grilleGlade[indiceLigne][index].set_label("")
                    c.effacerReponse()
                end
            end
        }
    end
    ##
    #   Effacer l'ensemble des réponses présentent dans la grille
    def effacerToutesLesReponses()
        for indiceLigne in 0..4
            effacerLigneReponse(indiceLigne)
        end
        @grilleGlade[0][0].set_label("\n\n                ")
    end

    ##
    #   Cette methode permet de sauvegarder une grille sous format json
    #   dans un fichier json correspondant à l'utilisateur
    def sauvegarder()
        casesJson = "" 
        for cases in @grille
            for uneCase in cases
                if(uneCase != nil)
                    casesJson += uneCase.json()
                end
            end
        end
        fichierGrille = File.new(@fichierGrille , "w")
        if(@tempsChrono.inject(:+) != 0)
            contenu = @grilleJson.jsonGrille(@grilleJson.jsonTailleGrille(@taille) + @grilleJson.jsonChronometre(@tempsChrono[0],@tempsChrono[1],@tempsChrono[2]) + @grilleJson.jsonCases(casesJson.chomp(',')))
        elsif
            contenu = @grilleJson.jsonGrille(@grilleJson.jsonTailleGrille(@taille) + @grilleJson.jsonCases(casesJson.chomp(',')))
        end
        parseJson = JSON.parse(contenu)
        fichierGrille.write(JSON.pretty_generate(parseJson))
        fichierGrille.close()
    end

    ##
    #   Permet de charger le fichier json contenant la grille
    def charger()
        if(@fichierGrille != nil)
            contenuFichierGrille = File.read(@fichierGrille)
            grilleParseJson = JSON.parse(contenuFichierGrille)
            @taille = grilleParseJson["grille"]["taille"]
            @grille = Array.new(@taille){Array.new(@taille)}
            i,j = 0,0
            #initialiser la grille
            for uneCase in grilleParseJson["grille"]["cases"]
                if(uneCase.key?("neutre"))
                    # Ajouter case neutre à la grille
                    @grille[i][j] = CaseNeutre.creer(@grille,"Gris", i, j)
                    #puts("On ajoute  la case neutre" + uneCase.to_s)
                elsif(uneCase.key?("reponse"))
                    # Ajouter case réponse à la grille
                    @grille[i][j] = CaseReponse.creer(@grille,"Blanc", i, j, uneCase["corrige"])
                    @grille[i][j].reponse = uneCase["reponse"]
                    @grille[i][j].hypotheses = uneCase["hypotheses"]
                    #puts("On ajoute  la case réponse" + uneCase.to_s)
                elsif(uneCase.key?("somme"))
                    # Ajouter case somme à la grille
                    @grille[i][j] = CaseSomme.creer(@grille,"Bleu", i, j, uneCase["somme"]["valeurHorizontale"], uneCase["somme"]["nbCasesHorizontales"], uneCase["somme"]["sommeVerticale"],  uneCase["somme"]["nbCasesVerticales"])
                    #puts("On ajoute  la case somme" + uneCase.to_s)
                else
                    raise "[chargerGrille] Case inconnue, impossible de l'ajouter à la grille"
                end
                j = (j+1) % @taille
                if(j == 0)
                    i+=1
                end
            end
            if(grilleParseJson["grille"].has_key?("chronometre"))
                @tempsChrono = grilleParseJson["grille"]["chronometre"]
            end
        else
            print("@fichierGrille : Chargement impossible car fichier inexistant.")
        end
    end
    ##
    #   Redéfinition de la méthode to_s pour afficher la grille 
    def to_s()
        for cases in @grille
            for uneCase in cases
                if(uneCase != nil)
                    print(uneCase.to_s + ",\n")
                else
                    print("vide,\n")
                end
            end
        end
    end

end