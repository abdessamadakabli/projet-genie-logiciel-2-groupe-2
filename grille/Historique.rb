
require "../cases/CaseNeutre.rb"
require "../cases/CaseReponse.rb"
require "../cases/CaseSomme.rb"
require "../grille/Grille.rb"
# Une action serait une chaîne de caractère par exemple :
# On saisit dans la case [3][6] la valeur 4 :
# saisie_reponse:3:6:4
# On saisir dans la case [2][5] l'hypothèse 5 :
# saisie_hypothese:2:5:5
# On efface la réponse de la case [3][6] (qui était 4):
# effacer_reponse:3:6:4
# On efface l'hypothèse 5 de la case [2][5] :
# effacer_hypothese:2:5:5
# On veut effacer TOUTES les hypothèses de la case [5][7] (qui étaient 1,5,7) :
# effacer_hypotheses:5:7:1:5:7

##
#   L'Historique d'actions du joueur lors d'une partie
#   Afin de pouvoir effectuer des undo / redo et de charger la partie
class Historique

    attr_writer :taille
    attr_reader :nom_Action, :iterateur, :grille

    ##
    #   Redéfinition de new() car l'Historique de coups doit connaître la grille du Kakuro
    #   @param grille la grille (matrice) du Kakuro contenant les cases
    def Historique.creer(grille)
        new(grille)
    end
    private_class_method:new

    ##
    #   Constructeur de l'Historique
    #   @param grille la grille (matrice) du Kakuro contenant les cases
    def initialize(grille)
        @grille = grille
        @nom_Action = Array.new()
        @taille = 0
        @iterateur = 0
    end


    ##
    #   Permet d'ajouter une action dans la liste de l'Historique
    #   @param new_Action une nouvelle action à inscrire dans l'historique
    def ajout_Action(new_Action)
        @nom_Action.push(new_Action)
        @taille += 1
        @iterateur = @taille - 1
    end

    ##
    #   Effectue un undo (retour en arrière) des actions du joueur
    #   agît sur la grille de jeu et la partie GTK3 affichée au joueur
    def undo()
        if (@iterateur >= 0)
            action = @nom_Action[@iterateur].split(':')
            ac1 = action[1].to_i
            ac2 = action[2].to_i
            case action[0]
            when "saisie_reponse"
                @grille.getCase(ac1,ac2).effacerReponse()
                @grille.grilleGlade[ac1][ac2].set_label("")
            when "saisie_hypothese"
                @grille.getCase(ac1,ac2).effacerUneHypothese(action[3].to_i)
                hypo_to_s(ac1,ac2)
            when "effacer_reponse"
                @grille.getCase(ac1,ac2).saisirReponse(action[3].to_i)
                @grille.grilleGlade[ac1][ac2].set_label(action[3].to_s)
            when "saisie_hypothese"
                @grille.getCase(ac1, ac2).effacerUneHypothese(action[3].to_i)
                hypo_to_s(ac1,ac2)
            when "effacer_hypotheses"
                laTaille = action.length() - 3
                it = 3
                while(laTaille > 0)
                    @grille.getCase(ac1, ac2).saisirHypotheses(action[it].to_i)
                    laTaille -= 1
                    it += 1
                    hypo_to_s(ac1,ac2)
                end
            else
                puts "Cas non traite par undo"
            end
            @iterateur -= 1
        end
    end

    ##
    #   Effectue un redo (refaire) des actions du joueur
    #   agît sur la grille de jeu et la partie GTK3 affichée au joueur
    def redo()
        if (@iterateur != @nom_Action.length()-1)
            @iterateur += 1
            action = @nom_Action[@iterateur].split(':')
            ac1 = action[1].to_i
            ac2 = action[2].to_i
            case action[0]
            when "saisie_reponse"
                @grille.getCase(ac1, ac2).saisirReponse(action[3].to_i)
                @grille.grilleGlade[ac1][ac2].set_label(action[3].to_s)
            when "saisie_hypothese"
                @grille.getCase(ac1, ac2).saisirHypotheses(action[3].to_i)
                hypo_to_s(ac1,ac2)
            when "effacer_reponse"
                @grille.getCase(ac1, ac2).effacerReponse()
                @grille.grilleGlade[ac1][ac2].set_label("")
            when "saisie_hypothese"
                @grille.getCase(ac1, ac2).effacerUneHypothese(action[3].to_i)
                hypo_to_s(ac1,ac2)
            when "effacer_hypotheses"
                @grille.getCase(ac1, ac2).effacerUneHypothese(action[3].to_i)
                hypo_to_s(ac1,ac2)
            else
                puts "Cas non traite par redo"
            end      
        end
    end

    ##
    #   Permet de récupérer la chaine de caractère de l'hypothese pour la case I et J
    #   @param i La ligne sur laquelle se la case
    #   @param j La colonne sur laquelle se trouve la case
    def hypo_to_s(i,j)
        chaine = String.new()
        if (@grille.getCase(i, j).existeHypothese?(1))
            chaine += "1"
        else
            chaine += " "
        end
        chaine += "  "
        if (@grille.getCase(i, j).existeHypothese?(2))
            chaine += "2"
        else
            chaine += " "
        end
        chaine += "  "
        if (@grille.getCase(i, j).existeHypothese?(3))
            chaine += "3"
        else
            chaine += " "
        end
        chaine += "\n"
        if (@grille.getCase(i, j).existeHypothese?(4))
            chaine += "4"
        else
            chaine += " "
        end
        chaine += "  "
        if (@grille.getCase(i, j).existeHypothese?(5))
            chaine += "5"
        else
            chaine += " "
        end
        chaine += "  "
        if (@grille.getCase(i, j).existeHypothese?(6))
            chaine += "6"
        else
            chaine += " "
        end
        chaine += "\n"
        if (@grille.getCase(i, j).existeHypothese?(7))
            chaine += "7"
        else
            chaine += " "
        end
        chaine += "  "
        if (@grille.getCase(i, j).existeHypothese?(8))
            chaine += "8"
        else
            chaine += " "
        end
        chaine += "  "
        if (@grille.getCase(i, j).existeHypothese?(9))
            chaine += "9"
        else
            chaine += " "
        end
        @grille.grilleGlade[i][j].set_label(chaine)
    end

end