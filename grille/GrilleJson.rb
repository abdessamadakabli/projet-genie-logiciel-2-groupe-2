#   version : 0.1
require 'json'
require 'gtk3'
require 'date'
##
#   Cette classe permet de charger une grille depuis des fichiers Json 
#   Elle permet également de stocker des grilles en fichier Json
class GrilleJson < Gtk::Builder
    ##
    #   Ce constructeur permet d'initialiser le parseur de grilles
    def initialize()
        super()
		self.add_from_file("../glade/GenerationGrille.glade")
        # Creation d'une variable d'instance par composant identifié dans glade
		#puts "Création des variables d'instances"
		self.objects.each() { |p| 	
				unless p.builder_name.start_with?("___object") 
					#puts "\tCreation de la variable d'instance @#{p.builder_name}"
					instance_variable_set("@#{p.builder_name}".intern, self[p.builder_name]) 
				end
		}
        #puts "Connexion des signaux"
		self.connect_signals { |handler| 
				begin
					method(handler) 
				rescue	
					puts "[Attention] Vous devez definir la methode #{handler} :\tdef #{handler}....\tend"
					self.class.send( :define_method, handler.intern) {
						puts "La methode #{handler} n'est pas encore définie.. Arrêt"
						Gtk.main_quit
					}
					retry
				end
		}
        @fenetreGestionnaireGrille.set_resizable(true)
		@fenetreGestionnaireGrille.signal_connect('destroy') { 
			puts "Au Revoir !!!"; Gtk.main_quit 
		}
        @casesJson = ""
        connexionSignaux()
    end
    ##
    #   new étant privée, cette méthode permet de créer le parseur de la grille
    def GrilleJson.creer()
        new()
    end

    ##
    #   Permet de générer le code json de la grille
    #   @param contenu Le contenu de la grille (taille, cases)
    def jsonGrille(contenu)
        json = "{\t\"grille\": { #{contenu}}}"
        return json
    end

    ##
    #   Permet de géréner le code json correspondant à la taille de la grille
    #   @param taille La taille de la grille
    def jsonTailleGrille(taille)
        json = "\"taille\": #{taille}," 
        return json
    end
    ##
    #   Permet de géréner le code json correspondant aux cases de la grille
    #   @param contenu L'ensemble des cases au format json
    def jsonCases(contenu)
        json = "\"cases\": [#{contenu}]" 
        return json
    end

    ##
    #   Permet de créer le code json correspondant à la case neutre
    def jsonCaseNeutre()
        json = "{\"neutre\" : -1 },"
        return json
    end

    ##
    #   Permet de créer le code json correspondant à la case réponse
    #   @param corrige le corrige
    #   @param reponse la reponse
    #   @param hypotheses l hypothes
    def jsonCaseReponse(corrige, reponse, hypotheses)
        if (reponse == nil)
            reponse = -1
        end
        json = "{\"reponse\" : #{reponse},"
        json += "\"corrige\" : #{corrige},"
        json += "\"hypotheses\" : #{hypotheses} },"
        return json
    end

    ##
    #   Transformer le chronomètre en JSON
    #   @param heures les heures
    #   @param minutes les minutes
    #   @param secondes les secondes
    def jsonChronometre(heures, minutes, secondes)
        json = "\"chronometre\" : #{[heures,minutes,secondes]},"
        return json
    end

    ##
    #   Permet de créer le code json correspondant à la case somme
    #   @param sommeVerticale La somme à effectuer verticalement
    #   @param nbCasesHorizontale Le nombre de cases associées à la somme verticale 
    #   @param sommeVerticale La somme à effectuer horizontalement
    #   @param nbCasesHorizontales Le nombre de cases associées à la somme horizontale
    def jsonCaseSomme(sommeVerticale, nbCasesVerticales, sommeHorizontale, nbCasesHorizontales)
        json = "{\"somme\": {
                        \"sommeVerticale\": #{sommeVerticale},
                        \"nbCasesVerticales\": #{nbCasesVerticales},
                        \"valeurHorizontale\": #{sommeHorizontale},
                        \"nbCasesHorizontales\": #{nbCasesHorizontales}}},"
        return json
    end

    ##
    #   Permet de créer le fichier json de la grille
    #   @param nomFichier Le nom du fichier json à créer
    #   @param contenuJson Le contenu avec le quel on remplit le fichier
    def jsonCreerFichierGrille(nomFichier, contenuJson)
        fichierGrille = File.new(nomFichier+".json", "w")
        parseJson = JSON.parse(contenuJson)
        fichierGrille.write(JSON.pretty_generate(parseJson))
        fichierGrille.close()
    end

    ##
    #   Permet d'initialiser l'ensemble des signaux relatives à la création de grille
    def connexionSignaux()
        @btnAjoutCase.signal_connect('clicked'){
            saisieTypeCase     = @saisieTypeCase.active_text()
            if(saisieTypeCase == "Case neutre") 
                @casesJson += jsonCaseNeutre()
                @lblInfoAction.set_text("[Message] : La case neutre a bien été créée.")
            elsif(saisieTypeCase == "Case réponse")
                valeurCase =  @saisieValeurCase.text()
                if(!valeurCase.empty?)
                    hypotheses = Array.new(9, false)
                    @casesJson += jsonCaseReponse(valeurCase, -1, hypotheses)
                    @lblInfoAction.set_text("[Message] : La case réponse a bien été créée (#{valeurCase}).")
                else
                    @lblInfoAction.set_text("[Message] : Le champs valeurCase est vide.")
                end
            elsif(saisieTypeCase == "Case somme")
                sommeVerticale = @saisieSommeVerticale.text()
                nbCasesVerticales = @saisieNbCasesVerticales.text()
    
                sommeHorizontale = @saisieSommeHorizontale.text()
                nbCasesHorizontales = @saisieNbCasesHorizontales.text()

                if(!sommeVerticale.empty? and !nbCasesVerticales.empty? and !sommeHorizontale.empty? and !nbCasesHorizontales.empty?)
                    @casesJson += jsonCaseSomme(sommeVerticale, nbCasesVerticales, sommeHorizontale, nbCasesHorizontales)
                    @lblInfoAction.set_text("[Message] : La case somme a bien été créée (#{sommeVerticale},#{nbCasesVerticales},#{sommeHorizontale},#{nbCasesHorizontales}).")
                else
                    @lblInfoAction.set_text("[Message] : Un ou plusieurs champs associés à la case somme est/sont vides.")
                end
            end

        }
        @btnGenererGrille.signal_connect('clicked'){
            cheminGrilles = "../jsonGrilles/"
            tailleGrille = @saisieTailleGrille.text()
            if(!tailleGrille.empty?)
                date = Time.new
                dateActuelle = date.day.to_s + "_" + date.month.to_s + "_" + date.year.to_s + "_" + date.hour.to_s + "_" + date.min.to_s
                nomFichier = cheminGrilles + "Grille_" +  dateActuelle
                jsonCreerFichierGrille(nomFichier, jsonGrille(jsonTailleGrille(tailleGrille) + jsonCases(@casesJson.chomp(','))))
                @lblInfoAction.set_text("[Message] : Le fichier #{nomFichier} a bien été créé.")
            else
                @lblInfoAction.set_text("[Message] : Le champs tailleGrille est vide.")
            end
        }
    end
    
    ##
    #   Permet d'afficher la fenetre d'edition
    def afficherFenetreEdition()
        @fenetreGestionnaireGrille.show_all
    end

    private_class_method :new
end