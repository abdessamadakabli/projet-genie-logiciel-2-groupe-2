load ("../Page.rb")
##
#   Cette classe permet de définir la fenetre menu principal 
#   et l'ensemble des interactions qui lui sont associés
class MenuPrincipale < Page
   
    ##
    # new étant privée on rédéfinit
    def MenuPrincipale.creer()
        new()
    end

    ##
    #   Permet de set le @titre
    #   @param texte le titre
    def setTitre(texte)
        @titre.set_label(texte) 
    end

    ##
    #   Permet de set la sensibilite du bouton nouvelle partie
    #   @param b boolean pour choisir la sensibilite
    def setBtnNouvellePartieSensibilite(b)
        @BTN_nouvellePartie.sensitive = b
    end

    ##
    #   Permet de set la sensibilite du bouton charger partie
    #   @param b boolean pour choisir la sensibilite
    def setBtnChargerPartieSensibilite(b)
        @BTN_chargerPartie.sensitive = b
    end

    ##
    #   Permet d'initialiser les signaux
    def initialiserSignaux()
        @BTN_nouvellePartie.sensitive = false
        @BTN_chargerPartie.sensitive = false

        @BTN_nouvellePartie.signal_connect('clicked'){
            cacher
            @NouvellePartie.afficher
        }
        @BTN_chargerPartie.signal_connect('clicked'){
            cacher
            @ChargerPartie.afficher
        }
        @BTN_options.signal_connect('clicked'){
            cacher
            @Option.afficher(self)
        }
        @BTN_classements.signal_connect('clicked'){
            cacher
            @Classement.afficher()
        }
        @btnProfil.signal_connect('clicked'){
            cacher
            @Profil.afficher
        }
        @BTN_aPropos.signal_connect('clicked'){
            cacher
            @Apropos.afficher
        }
        @BTN_quitter.signal_connect('clicked'){
            Gtk.main_quit 
        }
    end

    ##
    #   Permet de passer à une autre langue
    #   @param infos les infos
    def swapLangue(infos)
        @BTN_nouvellePartie.set_label(infos["Menu Principal"]["Nouvelle Partie"])
		@BTN_chargerPartie.set_label(infos["Menu Principal"]["Charger Partie"])
		@BTN_options.set_label(infos["Menu Principal"]["Options"])
		@BTN_classements.set_label(infos["Menu Principal"]["Classements"])
		@BTN_aPropos.set_label(infos["Menu Principal"]["A Propos"])
		@BTN_quitter.set_label(infos["Menu Principal"]["Quitter"])
    end
end