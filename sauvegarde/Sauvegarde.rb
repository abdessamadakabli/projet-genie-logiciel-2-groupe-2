require '../profil/Profil.rb'
require '../grille/Grille.rb'

##
#   Cette classe permettra de gérer l'interface graphique de sauvegarde
#   Cette interface met à disposition de l'utilisateur l'ensemble des grilles
#   du jeu, qui ont été sauvegardées. Elle permet également à l'utilisateur de lancer une partie
class Sauvegarde < Page

    ##
    #   Permet d'établir les chemins vers les profils stockés en json
    CHEMIN_PROFILS           = "../jsonProfils/"
    ##
    #   Permet d'établir le chemins vers les grilles des différents profils stockés en json
    CHEMIN_PROFIL_GRILLES    = "/grilles/"
    ##
    #   Permet d'établir le chemins vers les grilles initiales
    CHEMIN_GRILLES_INITIALES = "../jsonGrilles/"

    ##
    #   Permet d'initialiser le gestionnaire de sauvegarde avec un profil
    #   @param profil Le profil actuellement choisi
    def initialize(profil)
        @profil = profil
    end
    ##
    #   new étant privée, on utilise cette méthode pour créer un pavé numérique
    #   @param profil Le profil actuellement choisi
    def Sauvegarde.creer(profil)
        new(profil)
    end
    ##
    #   Permet de mettre à jour la langue de toutes les pages
    #   @param infos Le JSON actuel
    def swapLangue(infos)
        @lstSauvegardes.set_text(infos["Sauvegarde"]["Liste Sauvegarde"])
        @titre.set_text(infos["Sauvegarde"]["Sauvegardes"])
        #@entry_nouveauNom.set_text(infos["Sauvegarde"]["Nom Fichier"])

		@btnRetour.set_label(infos["Sauvegarde"]["Retour"])
        @btnAides.set_label(infos["Sauvegarde"]["Aides"])
        @btnCharger.set_label(infos["Sauvegarde"]["Charger sauvegarde"])
        @btnSupprimer.set_label(infos["Sauvegarde"]["Supprimer sauvegarde"])
        @btnModifierNom.set_label(infos["Sauvegarde"]["Modifier nom"])
    end
    ##
    #   Permet d'afficher et récupérer toutes les sauvegardes du jeu
    def getToutesLesSauvegardes()
        @comboBoxSauvegardes.remove_all()
        for nomSauvegarde in @profil.nomsSauvegardesGrilles
            if(nomSauvegarde[2] == true)
                if(nomSauvegarde[1] != "")
                    @comboBoxSauvegardes.append_text(nomSauvegarde[1])
                else
                    @comboBoxSauvegardes.append_text(nomSauvegarde[0])
                end
            end
        end
    end
    ##
    #   Cette méthode permet de modifier le nom d'une sauvegarde
    #   - Actualiser la liste des sauvegardes
    #   - S'assurer que le nom n'existe pas 
    #   - Sauvegarde au bon indice
    #   - Modifier le profil json
    def modifierNomSauvegarde()
        nom = @saisieNouveauFichier.text()
        cpt = 0

        if(nom != nil)
            if(!@profil.nomsSauvegardesGrilles.include?(nom))
                for fichier in @profil.nomsSauvegardesGrilles
                    if fichier[0] == @comboBoxSauvegardes.active_text
                        @profil.nomsSauvegardesGrilles[cpt][1] = nom
                        @lblMessageInfos.set_text("[Message] : Le nom de votre sauvegarde a été modifié.")
                    else
                        cpt += 1
                    end
                end
            else
                @lblMessageInfos.set_text("[Message] : Ce nom de sauvegarde existe déjà.")
            end
        else
            @lblMessageInfos.set_text("[Message] : Veuillez saisir le nom que vous souhaitez donner à votre sauvegarde.")
        end

        @profil.sauvegarderProfil()
        getToutesLesSauvegardes()
    end
    ##
    #   Permet des upprimer une sauvegarde
    #   - Convertir le nom de sauvegarde en nom de grille fichier
    #   - Supprimer (rm)
    def supprimerSauvegarde()
        indexNomGrille = 0
        index = 0
        cheminProfil = CHEMIN_PROFILS+@profil.nom+CHEMIN_PROFIL_GRILLES
        if @comboBoxSauvegardes.active_text != nil
            begin
                nomFichiersGrillesProfil = Dir.entries("#{CHEMIN_PROFILS}/#{@profil.nom}/#{CHEMIN_PROFIL_GRILLES}").select { |nomFic| File.extname(nomFic) == ".json" }
                for nomSauvegarde in @profil.nomsSauvegardesGrilles
                    if(nomSauvegarde[1] != "")
                        if(nomSauvegarde[1] == @comboBoxSauvegardes.active_text)
                            indexNomGrille = index
                        end
                    else
                        if(nomSauvegarde[0] == @comboBoxSauvegardes.active_text)
                            indexNomGrille = index
                        end
                    end
                    index += 1
                end
                FileUtils.remove_file(cheminProfil + nomFichiersGrillesProfil[indexNomGrille])
                FileUtils.cp(CHEMIN_GRILLES_INITIALES+nomFichiersGrillesProfil[indexNomGrille], cheminProfil)
                @profil.nomsSauvegardesGrilles[indexNomGrille][1] = ""
                @profil.nomsSauvegardesGrilles[indexNomGrille][2] = false
                @lblMessageInfos.set_text("[Message] : La sauvegarde #{@comboBoxSauvegardes.active_text} a été supprimé.")
            rescue Errno::ENOENT
                @lblMessageInfos.set_text("[Message] : Il n'y a aucune sauvegarde à supprimer.")
            end
        else
            @lblMessageInfos.set_text("[Message] : Veuillez sélectionner la sauvegarde à supprimer.")
        end
        getToutesLesSauvegardes()
        @profil.sauvegarderProfil()
    end

    ##
    #   Cette méthode permet de charger une sauvegarde d'une grille dans le profil
    def chargerSauvegarde()
        index = 0
        indexNomGrille = 0
        cheminProfil = CHEMIN_PROFILS+@profil.nom+CHEMIN_PROFIL_GRILLES
        if @comboBoxSauvegardes.active_text != nil
            nomFichiersGrillesProfil = Dir.entries("#{CHEMIN_PROFILS}/#{@profil.nom}/#{CHEMIN_PROFIL_GRILLES}").select { |nomFic| File.extname(nomFic) == ".json" }
            for nomSauvegarde in @profil.nomsSauvegardesGrilles
                if(nomSauvegarde[1] != "")
                    if(nomSauvegarde[1] == @comboBoxSauvegardes.active_text)
                        indexNomGrille = index
                    end
                else
                    if(nomSauvegarde[0] == @comboBoxSauvegardes.active_text)
                        indexNomGrille = index
                    end
                end
                index += 1
            end

            @grille =  Grille.creer(cheminProfil.to_s + nomFichiersGrillesProfil[indexNomGrille].to_s)
            @profil.grille = @grille
            #print("Fichier Grille : " + @grille.fichierGrille.to_s + "\n")
            cacher
            @Partie5.profil = @profil 
            mode = @grille.fichierGrille.split("/")[4].split("_")[0]
            difficulte = @grille.fichierGrille.split("/")[4].split("_")[2].chomp(".json")
            if(mode == "competitive")
                mode = "competition"
            end
            @Partie5.afficher(mode,difficulte)
            @lblMessageInfos.set_text("[Message] : La grille #{nomFichiersGrillesProfil[indexNomGrille]} a été chargée dans le profil.")
        end
        getToutesLesSauvegardes()
    end

    ##
    #   Afficher
    def afficher()
        super()
        getToutesLesSauvegardes()
    end
    ##
    #   On initialise les boutons Gtk
    def initialiserSignaux()  
        @fenetreSauvegarde.remove(@base)
        @comboBoxSauvegardes.signal_connect('popdown'){
            getToutesLesSauvegardes()
        }
        @btnModifierNom.signal_connect('clicked'){
            modifierNomSauvegarde()
        }

        @btnCharger.signal_connect('clicked'){
            chargerSauvegarde()
        }

        @btnSupprimer.signal_connect('clicked'){
            supprimerSauvegarde()
        }

        @btnRetour.signal_connect('clicked'){     
            cacher
            @MenuPrincipale.afficher
        }
    end
end