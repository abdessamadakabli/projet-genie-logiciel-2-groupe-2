##
#   Classe permettant de créer la page gérant la pause
class Pause < Page

    ##
    #   Permet de créer la fenêtre de pause
    #   @param profil le profil en cours de traitement
    def Pause.creer(profil)
        new(profil)
    end

    ##
    #   Permet d'initialiser la fenêtre de pause
    #   @param profil le profil en cours de traitement
    def initialize(profil)
        @profil = profil
    end

    ##
    #   Permet d'initialiser les signaux de la page
    def initialiserSignaux
        @fenetrePause.remove(@base)

        @btn_menuenjeu_retour.signal_connect("clicked") {
            cacher
            @partie.afficher(@partie.getMode.downcase, @partie.getDifficulte.downcase)
        }

        @btn_quitter.signal_connect("clicked") {
            cacher
            @MenuPrincipale.afficher
            indice =  @profil.getIndiceGrille(@profil.grille.fichierGrille)
            if(indice != -1)
                @profil.nomsSauvegardesGrilles[indice][2] = true 
            end
            @profil.sauvegarderProfil()
            @profil.grille.sauvegarder()
            @partie.chronometre.pause()
        }

        @btn_options.signal_connect("clicked") {
            cacher
            @Option.afficher(self)
        }
    end

    ##
    #   Permet de set le @partie
    #   @param partie la partie
    def setPartie(partie)
        @partie = partie
    end

    ##
    #   Permet de changer la langue de la page
    #   @param infos les infos du fichier JSON
    def swapLangue(infos)
        @label_pause.set_text(infos["Menu en Jeu"]["Menu"])
		@btn_options.set_label(infos["Menu en Jeu"]["Options"])
		@btn_quitter.set_label(infos["Menu en Jeu"]["Quitter"])
		@btn_menuenjeu_retour.set_label(infos["Menu en Jeu"]["Retour"])
    end
end