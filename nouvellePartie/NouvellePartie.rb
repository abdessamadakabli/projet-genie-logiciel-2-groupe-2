##
#   Classe permettant de gérer la création des nouvelles parties 
class NouvellePartie < Page

    ##
    #   Permet de créer la fenetre nouvelle partie
    #   @param profil La fenetre nouvelle partie connaît le profil
    def NouvellePartie.creer(profil)
        new(profil)
    end


    ##
    #   Permet d'initialiser la fenêtre nouvelle partie
    #   @param profil le profil en cours d'utilisation
    def initialize(profil)
        @profil = profil
    end
   
    ##
    #   Permet d'initialiser les signaux
    def initialiserSignaux()
        @fenetreNouvellePartie.remove(@base)

        @BTN_retour.signal_connect('clicked'){     
            cacher
            @MenuPrincipale.afficher
        }

        @BTN_tutoriel.signal_connect('clicked'){
            cacher
            @profil.grille = Grille.creer("../jsonProfils/" + @profil.nom + "/grilles/tutoriel_1_facile.json")
            @Partie5.afficher("tutoriel", "facile") 
        }
        @BTN_bfc.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("fc")
            @FenetreSelectionGrilles.afficher()
        }
        @BTN_bnc.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("nc")
            @FenetreSelectionGrilles.afficher()
        }
        @BTN_bdc.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("dc")
            @FenetreSelectionGrilles.afficher()
        }

        @BTN_bfo.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("fo")
            @FenetreSelectionGrilles.afficher()
        }
        @BTN_bno.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("no")
            @FenetreSelectionGrilles.afficher()
        }
        @BTN_bdo.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("do")
            @FenetreSelectionGrilles.afficher()
        }

        @BTN_bfp.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("fp")
            @FenetreSelectionGrilles.afficher()
        }
        @BTN_bnp.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("np")
            @FenetreSelectionGrilles.afficher()
        }
        @BTN_bdp.signal_connect('clicked'){
            cacher
            @FenetreSelectionGrilles.setNiveau("dp")
            @FenetreSelectionGrilles.afficher()
        }
    end

    ##
    #   Permet de passer à une autre langue
    #   et difficultés de la grille
    #   @param infos Les informations relatives aux modes 
    def swapLangue(infos)
        @modedejeu.set_text(infos["Nouvelle Partie"]["Mode de Jeu"])
        @classique.set_text(infos["Nouvelle Partie"]["Classique"])
        @competition.set_text(infos["Nouvelle Partie"]["Compétition"])
        @perfection.set_text(infos["Nouvelle Partie"]["Perfection"])

		@BTN_tutoriel.set_label(infos["Nouvelle Partie"]["Tutoriel"])
		@BTN_retour.set_label(infos["Nouvelle Partie"]["Retour"])

        @BTN_bfc.set_label(infos["Nouvelle Partie"]["Facile"])
        @BTN_bnc.set_label(infos["Nouvelle Partie"]["Normal"])
        @BTN_bdc.set_label(infos["Nouvelle Partie"]["Difficile"])

        @BTN_bfo.set_label(infos["Nouvelle Partie"]["Facile"])
        @BTN_bno.set_label(infos["Nouvelle Partie"]["Normal"])
        @BTN_bdo.set_label(infos["Nouvelle Partie"]["Difficile"])

        @BTN_bfp.set_label(infos["Nouvelle Partie"]["Facile"])
        @BTN_bnp.set_label(infos["Nouvelle Partie"]["Normal"])
        @BTN_bdp.set_label(infos["Nouvelle Partie"]["Difficile"])
    end
end