require "../grille/Grille.rb"

##
#   Classe permettant à l'utilisateur de sélectionner parmis
#   les différentes grilles proposées, une grille selon
#   une difficulté donnée et un mode précis
class FenetreSelectionGrilles < Page
    ##
    #   Permet d'initialiser la fenêtre de sélection de grille
    #   @param profil La fenetre de sélection connait un profil
    def initialize(profil)
        super()
        @niveau  = ""
        @difficulte = ""
        @mode = ""
        @profil = profil
    end
    ##
    #   Permet de créer la fenêtre de sélection de grille
    #   @param profil La fenetre de sélection connait un profil
    def FenetreSelectionGrilles.creer(profil)
        new(profil) 
    end

    ##
    #   Permet de set le @niveau
    #   @param niveau le niveau
    def setNiveau(niveau)
        @niveau = niveau
    end

    ##
    #   Permet de construire le chemin du fichier JSON
    #   @param num le numero du niveau
    def construireChemin(num)
        niveau = ""
        case @niveau
        when "fc","nc","dc"
            @mode = "classique"    
            mode = "classique"       
        when "fo","no","do"
            @mode = "competition"
            mode = "competitive"
        when "fp","np","dp"
            @mode = "perfection"  
            mode = "perfection"
        end
        if(@niveau.include?("f"))
            @difficulte = "facile"
        elsif(@niveau.include?("n"))
            @difficulte = "moyen"
        elsif(@niveau.include?("d"))
            @difficulte = "difficile"
        end
        #print("../jsonProfils/" + @profil.nom + "/grilles/" + mode + "_" + num.to_s + "_" + @difficulte + ".json")
        return "../jsonProfils/" + @profil.nom + "/grilles/" + mode + "_" + num.to_s + "_" + @difficulte + ".json"
    end
    
    ##
    # Permet d'initialiser les signaux de la page
    def initialiserSignaux()
        @FenetreSelectionGrilles.remove(@base)

        @btn1.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(1))
            chargerFenetreDifficulte()
        }
        @btn2.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(2))
            chargerFenetreDifficulte()
        }
        @btn3.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(3))
            chargerFenetreDifficulte()
        }
        @btn4.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(4))
            chargerFenetreDifficulte()
        }
        @btn5.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(5))
            chargerFenetreDifficulte()
        }
        @btn6.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(6))
            chargerFenetreDifficulte()
        }
        @btn7.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(7))
            chargerFenetreDifficulte()
        }
        @btn8.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(8))
            chargerFenetreDifficulte()
        }
        @btn9.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(9))
            chargerFenetreDifficulte()
        }
        @btn10.signal_connect('clicked'){
            @profil.grille = Grille.creer(construireChemin(10))
            chargerFenetreDifficulte()
        }
        @btnRetour.signal_connect('clicked'){     
            cacher
            @NouvellePartie.afficher
        }
    end

    ##
    # Permet de charger la fenetre en fonction de la difficulte
    def chargerFenetreDifficulte()
        cacher
        case @difficulte
        when "facile"
            @Partie5.afficher(@mode, @difficulte)              
        when "moyen"
            @Partie7.afficher(@mode, @difficulte)
        when "difficile"
            @Partie10.afficher(@mode, @difficulte) 
        end
    end
    
end