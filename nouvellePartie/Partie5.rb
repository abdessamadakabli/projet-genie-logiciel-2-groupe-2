require "../grille/Grille.rb"
require "../partie/Informations.rb"
require "../cases/Case.rb"
require "../cases/CaseNeutre.rb"
require "../cases/CaseReponse.rb"
require "../cases/CaseSomme.rb"

class Partie5 < Page

    def initialize(profil)
        super()
        @profil = profil
        @is_Eraser = false
        @is_Pencil = false
    end

    def Partie5.creer(profil)
        new(profil)
    end

    def initialiserSignaux()
        #@fenetreKakuro.remove(@base)
        @boutonMenu.signal_connect('clicked'){
            #   Sauvegarde le chrono
            @chronometre.pause()
            @profil.grille.tempsChrono = [@chronometre.heures, @chronometre.minutes, @chronometre.secondes]
            cacher
            @Pause.setPartie(self)
            @Pause.afficher
        }
        connexionSignauxBarreOutils()
    end
    def initialiserLignesCasesBoutons(indiceLigneCasesBoutons)
        ##
        #   Gestion des boutons de la grille
        @grilleCasesGlade[indiceLigneCasesBoutons].each_with_index{ |c,index|
            if (@grilleJeu.getCase(indiceLigneCasesBoutons,index).class() == CaseReponse)
                c.set_name("white_case_rep")
                c.signal_connect('button_press_event') {|widget, event|
                if (event.button == 1)
                    if @is_Eraser == true
                        c.set_label("")
                        @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerReponse()
                        @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerHypotheses()
                    elsif @is_Pencil == true
                        if (@grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur == "white_case_rep")
                            c.set_name("green_case")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "green_case"
                        else
                            c.set_name("white_case_rep")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "white_case_rep"
                        end
                    else
                        c.set_name("yellow_case")
                        @PaveNumerique.afficher(@grilleJeu.getCase(indiceLigneCasesBoutons,index), c, @grilleJeu)
                    end
                elsif (event.button == 3)
                    if (@is_Eraser == true)
                        c.set_label("")
                        @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerReponse()
                        @grilleJeu.getCase(indiceLigneCasesBoutons,index).effacerHypotheses()
                    elsif (@is_Pencil == true)
                        if (@grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur == "white_case_rep")
                            c.set_name("yellow_case")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "yellow_case"
                        else
                            c.set_name("white_case_rep")
                            @grilleJeu.getCase(indiceLigneCasesBoutons,index).couleur = "white_case_rep"
                        end
                    else    
                        c.set_name("orange_case")
                        @PaveHypothese.afficher(@grilleJeu.getCase(indiceLigneCasesBoutons,index), c, @grilleJeu)
                    end
                end
            }
            end
        }
    end

    def initialiserBoutonsCasesGlade()
        @grilleCasesGlade = Array.new(5){Array.new(5)}
        @grilleCasesGlade[0][0] = @a1
        @grilleCasesGlade[0][1] = @b1
        @grilleCasesGlade[0][2] = @c1
        @grilleCasesGlade[0][3] = @d1
        @grilleCasesGlade[0][4] = @e1
        @grilleCasesGlade[1][0] = @a2
        @grilleCasesGlade[1][1] = @b2
        @grilleCasesGlade[1][2] = @c2
        @grilleCasesGlade[1][3] = @d2
        @grilleCasesGlade[1][4] = @e2
        @grilleCasesGlade[2][0] = @a3
        @grilleCasesGlade[2][1] = @b3
        @grilleCasesGlade[2][2] = @c3
        @grilleCasesGlade[2][3] = @d3
        @grilleCasesGlade[2][4] = @e3
        @grilleCasesGlade[3][0] = @a4
        @grilleCasesGlade[3][1] = @b4
        @grilleCasesGlade[3][2] = @c4
        @grilleCasesGlade[3][3] = @d4
        @grilleCasesGlade[3][4] = @e4
        @grilleCasesGlade[4][0] = @a5
        @grilleCasesGlade[4][1] = @b5
        @grilleCasesGlade[4][2] = @c5
        @grilleCasesGlade[4][3] = @d5
        @grilleCasesGlade[4][4] = @e5
        
        chaine = String.new()
        for i in 0..4
            for j in 0..4
                if (@grilleJeu.getCase(i,j).class() == CaseReponse)
                    @grilleCasesGlade[i][j].set_name("white_case")
                    @grilleJeu.grille[i][j].couleur = "white_case_rep"
                    @grilleCasesGlade[i][j].set_label("")
                    if(@grilleJeu.getCase(i,j).reponse != -1)
                        @grilleCasesGlade[i][j].set_label("#{@grilleJeu.getCase(i,j).reponse}")
                    else
                        if(@grilleJeu.getCase(i,j).existeHypotheses())
                            @grilleJeu.grille[i][j].couleur = "white_case_hyp"
                            for k in 1..9
                                if(@grilleJeu.getCase(i,j).existeHypothese?(k))
                                    chaine += k.to_s + "  "
                                else
                                    chaine += " "
                                end
                                if(k%3==0)
                                    chaine += "\n"
                                end
                            end
                        end
                        @grilleCasesGlade[i][j].set_label(chaine)
                        chaine = ""
                    end
                elsif (@grilleJeu.getCase(i,j).class() == CaseNeutre)
                    @grilleCasesGlade[i][j].set_name("blue_case")
                    @grilleCasesGlade[i][j].set_label("")
                elsif (@grilleJeu.getCase(i,j).class() == CaseSomme)
                    @grilleCasesGlade[i][j].set_name("blue_case")
                    @grilleCasesGlade[i][j].set_label("")
                    if(@grilleJeu.getCase(i,j).sommeHorizontale == 0)
                        sommeHorizontale = ""
                    else
                        sommeHorizontale = @grilleJeu.getCase(i,j).sommeHorizontale
                    end
                    if(@grilleJeu.getCase(i,j).sommeVerticale == 0)
                        sommeVerticale = ""
                    else
                        sommeVerticale = @grilleJeu.getCase(i,j).sommeVerticale
                    end
                    if(sommeHorizontale == "" and sommeVerticale == "")
                        @grilleCasesGlade[i][j].set_label("")
                    else
                        @grilleCasesGlade[i][j].set_label("#{sommeHorizontale} / #{sommeVerticale}")
                    end
                end
            end
        end
        
        @grilleJeu.setGrilleGlade(@grilleCasesGlade)
        for indiceLigneCasesBoutons in 0..4
            initialiserLignesCasesBoutons(indiceLigneCasesBoutons)
        end
    end

    def verifierLignesGrille(indiceLigne, a_gagne)
        @grilleCasesGlade[indiceLigne].each_with_index{ |c,index|
            if ( @grilleJeu.getCase(indiceLigne,index).verifierReponse() == true)
                c.set_name("green_case")   # On indique au joueur que la case est bien remplie avec la couleur Verte
            elsif (@grilleJeu.getCase(indiceLigne,index).existeReponse() == false)
                a_gagne = false             # Si l'on trouve au moins une erreur dans la grille de Kakuro alors le joueur ne peut pas avoir gagné
                c.set_name("red_case")  # On indique au joueur son erreur sur la case en la remplissant de Rouge
            end  
        }
        return a_gagne
    end

    def verifier_Grille()
        # vérification de la grille de jeu
        a_gagne = true       
        for indiceLignes in 0..4           # Par défaut, si l'on ne trouve aucunes erreurs le joueur aura gagné
            a_gagne = verifierLignesGrille(indiceLignes, a_gagne)
        end
        @info.incr_Tentative()
        return a_gagne
    end

     # GESTION DE LA BARRE D'OUTIL
     def connexionSignauxBarreOutils()

        @boutonVerifierGrille.signal_connect('clicked'){
            verifier_Grille()
        }

        @boutonBombe.signal_connect('clicked'){ # Bombe
            @grilleJeu.effacerToutesLesHypotheses()
        }
        
        @boutonGomme.signal_connect('clicked'){ # Gomme
            @is_Eraser = @is_Eraser==false
            @is_Pencil = false
        }

        @boutonAvant.signal_connect('clicked'){ # Undo
            @grilleJeu.historique.undo()
        }
        
        @boutonApres.signal_connect('clicked'){ # Redo
            @grilleJeu.historique.undo()
        }

        @boutonPoubelle.signal_connect('clicked'){ # Poubelle
            @grilleJeu.effacerToutesLesReponses()
        }
        
        @boutonCrayon.signal_connect('clicked'){ # Crayon couleur
            # Changer comportement du clique gauche et clique droit : gauche = vert /\ droite = jaune
            @is_Pencil = @is_Pencil == false
            @is_Eraser = !@is_Pencil
        }
    end

    def getMode()
        return @etiquetteValeurMode.text
    end

    def getDifficulte()
        return @etiquetteValeurDifficulte.text
    end

    def afficher(mode, difficulte)
        @fenetreKakuro.remove(@base)
        super()
        @info = Info.creer(mode.to_s, difficulte.to_s, @etiquetteValeurMode, @etiquetteValeurDifficulte, @etiquetteValeurTentative)
        @grilleJeu = @profil.grille()
        @chronometre = Chronometre.creer(@lblChronometre, @boutonNouvellePartie, @boutonPause)
        @chronometre.heures =  @profil.grille.tempsChrono[0]
        @chronometre.minutes =  @profil.grille.tempsChrono[1]
        @chronometre.secondes =  @profil.grille.tempsChrono[2]
        @chronometre.lancer()
        initialiserBoutonsCasesGlade()
        initialiserSignaux()
    end
end
