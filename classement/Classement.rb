require 'time'
require 'gtk3'
require 'json'
require 'fileutils'
require "../profil/Profil.rb"

##
#   Cette classe va permettre de créer , afficher et gérer les interactions avec le classement
class Classement < Page

    ##
    #   Permet d'établir les chemins vers les profils stockés en json
    CHEMIN_PROFILS = "../jsonProfils/"

    ##
    #   Permet de cérer un classement
    #   @param menuPrincipale La fenetre menu pricipale
    def Classement.creer(menuPrincipal)
        new(menuPrincipal)
    end
    ##
    #   Constructeur permet la création du classement
    #   @param menuPrincipale La fenetre menu pricipale
    def initialize(menuPrincipal)
        @fichierClassemet = nil

        @MenuPrincipale = menuPrincipal

        @mode = "Perfection"
        @moyen = true
        @facile = false
        @difficile = false
        @nomJoueurs = []
    end

    ##
    #   Permet d'initialiser les signaux
    def initialiserSignaux()
        @fenetreClassement.remove(@base)    

        ##
        #   Permet de changer le classement en fonction des boutons cliqués !

        ##
        #   Gestion des boutons du mode de jeu 

        @btnPerfection.signal_connect('clicked'){
            @mode = "Perfection"
            @btnPerfection.set_name("green_btn")
            @btnCompetition.set_name("")
            remplirClassement()
        }

        @btnCompetition.signal_connect('clicked'){
            @mode = "Competitif"
            remplirClassement()
            @btnPerfection.set_name("")
            @btnCompetition.set_name("green_btn")
        }
        
        ##
        #   Gestion des boutons de la difficulté
        @btnFacile.signal_connect('clicked'){
            @facile = !@facile
            if @facile
                @btnFacile.set_name("green_btn")
            else
                @btnFacile.set_name("")
            end
            remplirClassement()
        }
        
        @btnMoyen.signal_connect('clicked'){
            @moyen = !@moyen
            if @moyen
                @btnMoyen.set_name("green_btn")
            else
                @btnMoyen.set_name("")
            end
            remplirClassement()
        }
        
        @btnDifficile.signal_connect('clicked'){
            @difficile = !@difficile
            if @difficile
                @btnDifficile.set_name("green_btn")
            else
                @btnDifficile.set_name("")
            end
            remplirClassement()
        }

        ##
        # Bouton Retour ! 
        @btnRetour.signal_connect('clicked'){
            cacher
            @MenuPrincipale.afficher()
        }
        @btnMoyen.set_name("green_btn")
        @btnPerfection.set_name("green_btn")
    end

    ##
    #   Permet de récupéré tout les profils
    def creerTableauProfils()
        @nomJoueurs = []
        nomsProfils = Dir.glob("#{CHEMIN_PROFILS}*")
        for nomProfil in nomsProfils
            @nomJoueurs.push(nomProfil.rpartition('/').last)
        end
    end

    ##
    #   Permet d'afficher le classement en le remplissant
    def afficher
        super
        remplirClassement
    end

    ##
    #   Permet de calculer une position en fonction du score   
    #   @param cat La catégorie du classement (Perfection / Competitive)
    def calculerPosition(cat)
        hash = {}      
        @nomJoueurs.each { |p|
            fichierProfil = File.read(CHEMIN_PROFILS + p+"/"+p+".json")
            infos = JSON.parse(fichierProfil)

            nbTrophee = calculerNb(infos, cat, "nbTrophees" + cat)
            nbPartie = calculerNb(infos, cat, "nbParties" + cat)
            score = 0
            if nbTrophee != 0 then
                score = nbPartie.to_f / nbTrophee.to_f
            end
            hash[p] = score
        }
        h = {}
        @nomJoueurs.each { |p|
            pos = 1
            score = hash[p]
            hash.each { |clef, valeur|
                if valeur > score then
                    pos += 1
                end
            }
            i = 1
            while i != 0 do
                i = 0
                h.each { |clef, valeur|
                    if valeur == pos then
                        i = 1
                        break;
                    end
                }
                if (i != 0) then
                    pos += 1
                end       
            end
            h[p] = pos
        }
        return h
    end

    ##
    #   Permet de calculer le nombre de trophée ou de partie d'un fichier JSON
    #   @param infos Les infos du fichier JSON
    #   @param cat La catégorie
    #   @param souscat La sous catégorie
    def calculerNb(infos, cat, souscat) 
        @temp = infos["profil"][cat][souscat]
        i = 0

        if @facile then
            i += @temp[0].to_i
        end
        if @moyen then
            i += @temp[1].to_i
        end
        if @difficile then
            i += @temp[2].to_i
        end

        return i
    end
    
    ##
    #   Permet de remplir le classement
    def remplirClassement()
        creerTableauProfils
        positions = calculerPosition(@mode)
        while true do
            if @gridTableauClassement.get_child_at(0,0) != nil then
                @gridTableauClassement.remove_row(0)
            else
                break
            end
        end

        @nomJoueurs.each { |p|
            begin
                fichierProfil = File.read(CHEMIN_PROFILS + p+"/"+p+".json")
                infos = JSON.parse(fichierProfil)

                pseudo = infos["profil"]["nom"]
                nbTrophee = calculerNb(infos, @mode, "nbTrophees" + @mode)
                nbPartie = calculerNb(infos, @mode, "nbParties" + @mode)

                @gridTableauClassement.attach(Gtk::Label.new(pseudo), 0, positions[p]-1, 1, 1)
                @gridTableauClassement.attach(Gtk::Label.new(nbTrophee.to_s), 1, positions[p]-1, 1, 1)
                @gridTableauClassement.attach(Gtk::Label.new( positions[p].to_s), 2, positions[p]-1, 1, 1)
                @gridTableauClassement.attach(Gtk::Label.new(nbPartie.to_s), 3, positions[p]-1, 1, 1)

            rescue JSON::ParserError
                @lblMessageInfos.set_text("[Message] : Le profil n'a pas pu être chargé.")
            end
        } 
        @fenetre.show_all
    end


end