# Projet de Génie logiciel 2 en Ruby/Gtk - L3 informatique à l'université du Mans 2021

Dans le cadre d'un projet lié au module Génie logiciel 2, nous avons réalisé le jeu de puzzle <b>Kakuro</b>.
Le projet suit une approche objet, utilisant la notation UML.

# Scénario

Pour notre scénario, nous allons utiliser l'exemple d'un joueur débutant appelé Christophe.

Lors du lancement du jeu, Christophe saisi son pseudo "Christophe". Après validation, il se trouve sur le menu principal. Avant de se lancer dans des parties, il va dans les Options, où il règle les paramètres en choisissant sa langue et en ajustant la résolution à celle qui lui convient le mieux. Il commence par lancer une Nouvelle Partie. Plusieurs choix de mode de jeu s'offre à lui :  Tutoriel, Classique, Classé et Perfection. Il commence par le mode  Tutoriel. Ce mode de jeu lui permettra de découvrir les règles ainsi que les techniques de résolutions. 
Sur ce mode de jeu il utilise les aides comme bon lui semble car ceci l'aide à apprendre les mécanismes du jeu. 

Après un moment d'apprentissage, il désire commencer à s'affronter à des vrais puzzles ! C'est pourquoi il lance le mode de jeu Classique. Ici il a le choix de 3 difficultés : Facile, Moyen, Difficile. Il commence par le mode Facile pour bien débuter. Celui-ci reprend les principes mêmes du puzzle Kakuro, en apportant quelques fonctionnalités d'aide si Christophe a des difficultés. 

Durant sa partie, il clique sur le bouton d'aide pour décocher les aides qui lui sont pas nécessaire. Puis il commence par insérer un nombre sur la grille à l'aide d'une fenêtre composée de tout les chiffres. Ensuite il veux remplir des cases avec des chiffres candidats, il va donc cliquer sur Afficher un bloc magique, ce qui va mettre en sur-brillance certaines cases et lui indiqué les chiffres possibles.  

Mais ceci était sans compter, en pleine partie acharnée, sur son père Pierre qui l'appela pour aller dîner. Christophe ne voulant pas perdre sa progression, ouvra le menu Pause  puis cliqua sur le bouton Sauvegarder la partie. Sur cette page, il lui suffit de cliquer sur Emplacement sauvegarde vide  et la partie est sauvegarder. Il peut ainsi revenir dans le menu précédent et quitter la partie. 

Après des heures de jeu, Christophe est prêt à être le numéro un, c'est pourquoi il se lance dans le mode Classé, là où les points sont comptés, ainsi que la difficulté :  Moyen. Maintenant, il va pouvoir montrer à ses amis tout son talent ! Mais attention, dans ce mode de jeu le temps est important et les aides coûtent cher... Il va falloir les utiliser à bon escient.

A la fin de ses parties il retourne dans le menu principal, puis il clique sur le bouton Classement. Il ne se trouve pas sur le tableau des scores, ceci est normal, il n'a pas choisi le bon classement en fonction de la difficulté auquel il a participer. Il choisi la difficulté : Moyen puis se trouve sur le tableau des scores. Il est à la deuxième position avec un nombre de points bien inférieur à son père Pierre car il a joué plus de partie, mais a obtenu moins d'étoiles que lui.



# Contributeurs

Nathan Tessier - <i>Chef du groupe</i><br>
Samuel Duran   - <i>Documentaliste</i><br>
Abdessamad Akabli<br>
Guillaume Briffault<br>
Thomas Choquet<br>
Allan Henry<br>
Louis Lafay<br>
Simon Planchenault <br>

# Clients

Christophe Desprès : <a href="christophe.despres@univ-lemans.fr">christophe.despres@univ-lemans.fr</a><br>
Pierre Jacoboni : <a href="pierre.jacoboni@univ-lemans.fr">pierre.jacoboni@univ-lemans.fr</a>


