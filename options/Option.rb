##
# Classe pour le bloc affichant des informations sur la partie en cours
class Option < Page
    ##
    #   Le chemin vers le json contanant les options
    CHEMIN_OPTIONS = "../jsonOptions/"
    ##
    #   Le chemin vers le json contanant les différentes langues
    CHEMIN_LANGUES = "../jsonLangues/"
    ##
    #   new étant privée on redéfinit
    def Option.creer
        new()
    end
    ##
    #   On redéfinit initialize
    def initialize
        initTabLangue
        initTabResolution
    end

    ##
    #   Permet d'afficher la page
    #   @param retour la page de retour
    def afficher(retour)
        @Retour = retour
        super()
    end

    ##
    #   Permet d'initialiser les signaux
    def initialiserSignaux()
        @fenetreOption.remove(@base)

        @btn_retour.signal_connect('clicked'){
            cacher
            @Retour.afficher
        }

        ##
        # 	Gérer le bouton de plein ecran
        @fullScreen.signal_connect('toggled') {
            changeFullscreenMode
        }

        ##
        # 	Gérer la combobox de resolution
        @resolution.signal_connect('changed') {
            changerResolution
        } 

        ##
        # 	Gérer la combobox de langue
        @langue.signal_connect('changed') {
            chargerLangue
        } 
    end

    ##
    #   Permet de charger la langue voulu via un fichier JSON
	def chargerLangue()
        begin
            fichier = File.read(CHEMIN_LANGUES + @tablangues[@langue.active] + ".json")
            infos = JSON.parse(fichier)
            updateLangues(infos)
            updateFichierOption
        rescue JSON::ParserError
            print "Erreur charger le fichier de langue"
            Gtk.main_quit 
        end
    end

    ##
    #   Permet de modifier le plein ecran sur toute les pages
    def changeFullscreenMode()
        @resolution.sensitive = !(@resolution.sensitive?)
        if !@resolution.sensitive? then
            @fenetre.fullscreen
        else
            @fenetre.unfullscreen
        end
        changerResolution
    end

    ##
    #   Permet l'initialisation du tableau des résolution
    def initTabResolution
		@tabresolutions = []
		@tabresolutions << 800
		@tabresolutions << 1000
	end

    ##
    #   Permet l'initialisation du tableau des langues
	def initTabLangue
		@tablangues = []
		@tablangues << "Francais"
		@tablangues << "Anglais"
	end

    ##
    #   Permet de changer la résolution des pages
    def changerResolution
        @fenetre.set_size_request(@tabresolutions[@resolution.active] + 200, @tabresolutions[@resolution.active])
        updateFichierOption
    end

    ##
    #   Permet d'initialiser le jeu avec les parametres rentrer lors de la derniere session de jeu
    def initOptions
        fichierOptions = File.read(CHEMIN_OPTIONS + "options.json")
        infos = JSON.parse(fichierOptions)
        # Charger le dernier profil
        if infos["Profil"] != "__default__" && infos["Profil"] != "" then
            @Profil.nom = infos["Profil"].to_s
            if(@Profil.chargerProfil(@Profil.getCheminProfils.to_s + infos["Profil"].to_s + "/" + infos["Profil"].to_s + ".json"))
                @Profil.importerGrillesInitiales()
                @Profil.afficherProfil()
                @Profil.setChargerEtSupprimerSensibilite(true)
                @MenuPrincipale.setBtnNouvellePartieSensibilite(true)
                @MenuPrincipale.setBtnChargerPartieSensibilite(true)
                @MenuPrincipale.setTitre("Kakuro, bienvenue "+ @Profil.nom)
            end
        end

        # Options de plein ecran
        if infos["Plein ecran"] == "true" then
            # @resolution.sensitive = !(@resolution.sensitive?)
            # @fullScreen.active= true      
            # @fenetre.fullscreen
        end

        # Options de resolution
        @tabresolutions.each_with_index do |element,index|
            if element == infos["Resolution"].to_i then
                @resolution.active= index;
                break;
            end
        end
        @fenetre.set_size_request(infos["Resolution"].to_i + 200, infos["Resolution"].to_i)

        # Options de langue
        @tablangues.each_with_index do |element,index|
            if element == infos["Langue"] then
                @langue.active= index;
                break;
            end
        end
        begin
            fichierProfil = File.read(CHEMIN_LANGUES + infos["Langue"] + ".json")
            infos = JSON.parse(fichierProfil)
            updateLangues(infos)
        rescue JSON::ParserError
            print "Erreur charger le fichier de langue"
            Gtk.main_quit 
        end
    end

    ##
    #   Permet de mettre à jour la langue de toutes les pages
    #   @param infos les infos du fichier JSON
    def updateLangues(infos)
        swapLangue(infos)
        @MenuPrincipale.swapLangue(infos)
        @NouvellePartie.swapLangue(infos)
        @Apropos.swapLangue(infos)
        @ChargerPartie.swapLangue(infos)
        @Profil.swapLangue(infos)
        @Pause.swapLangue(infos)
        @Partie5.swapLangue(infos)
        @Partie7.swapLangue(infos)
        @Partie10.swapLangue(infos)
    end

    ##
    #   Permet de changer la langue de la page
    #   @param infos les infos du fichier JSON
    def swapLangue(infos)
        @label_option.set_text(infos["Options"]["Option"])
		@label_fullscreen.set_text(infos["Options"]["Plein écran"])
		@label_resolution.set_text(infos["Options"]["Résolution"])
		@label_langue.set_text(infos["Options"]["Langue"])
		@btn_retour.set_label(infos["Options"]["Retour"])
	end

    ##
    #   Permet de mettre à jour le fichier de sauvegarde des options
    def updateFichierOption
        fichier_s = CHEMIN_OPTIONS + "options.json"
        fichier = File.new(fichier_s, "w")
        #print("NOM  : #{@Profil.nom}\n")
        conf = 
        "{
        \"Langue\": \"#{@tablangues[@langue.active]}\",
        \"Resolution\": \"#{@tabresolutions[@resolution.active]}\",
        \"Plein ecran\": \"#{@fullScreen.active?}\",
        \"Profil\": \"#{@Profil.nom}\"
        }"
        parseJson = JSON.parse(conf)
        fichier.write(JSON.pretty_generate(parseJson))
        fichier.close()
    end
end