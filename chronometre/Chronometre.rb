require "gtk3"
##
#   Cette classe va permettre créer et gérer un chronomètre
class Chronometre 
    attr_accessor :secondes,:minutes,:heures,:lancer, :thread
    ##
    #   Constructeur du chronomètre qui permet d'initialiser le temps à 0.
    #   @param lblTemps Le label qui affiche le temps
    #   @param btnLancer Le bouton lancer
    #   @param btnPause Le bouton Pause
    #   @param modeTutoActif Vérifier si le mode tutoriel est actif
    def initialize(lblTemps, btnLancer, btnPause, modeTutoActif)
        @secondes,@minutes,@heures = 0,0,0
        @pause  = false
        @lance  = false

        @lblTemps   = lblTemps
        @btnLancer = btnLancer
        @btnPause  = btnPause

        @thread = Thread.new{
            @lance = true
            while(true)
                if(@pause)
                    Thread.stop
                else
                    gestionAffichage()
                    sleep(1)
                    ajouterSecondes(1)
                end
            end
        }
        @btnPause.signal_connect "clicked" do |_widget|
            if(@lance && !modeTutoActif)
                if(!@pause)
                    #puts "clique sur le Bouton Pause"
                    pause()  
                    #print(@btnPause.methods)

                else
                    #puts "clique sur le Bouton Reprendre"
                    reprendre()
                end
            end
        end
    end

    ##
    #   new étant privée, on utilise cette méthode pour créer un chronomètre
    #   @param lblTemps Le label qui affiche le temps
    #   @param btnLancer Le bouton lancer
    #   @param btnPause Le bouton Pause
    #   @param modeTutoActif Vérifier si le mode tutoriel est actif
    def Chronometre.creer(lblTemps, btnLancer, btnPause, modeTutoActif)
        new(lblTemps, btnLancer, btnPause, modeTutoActif)
    end

    ##
    # Permet de lancer le chronomètre
    def lancer()
    end

    ##
    #   Permet de mettre en pause le chronomètre
    def pause()
        @pause = true
        playImage = Gtk::Image.new(:file => "../glade/img/play-solid.svg")
        @btnPause.image = playImage
    end

    ##
    # Getter de pause du chronomètre
    def getPause()
        return @pause
    end

    ##
    #   Permet de relancer le chronomètre là où il a été mis en pause
    def reprendre()
        if(@pause)
            @pause = false
            pauseImage = Gtk::Image.new(:file => "../glade/img/pause-circle-regular.svg")
            @btnPause.image = pauseImage
            @thread.wakeup
        end
    end

    ##
    #   Permet de remettre le temps du chronomètre à 0.
    def reinitialiser()
        @secondes,@minutes,@heures = 0,0,0
        @lance = false
        gestionAffichage()
    end

    ##
    #   Permet d'ajouter des secondes au chronomètre
    #   @param nbSecondes Le nombre de secondes à ajouter
    def ajouterSecondes(nbSecondes)
        @secondes += 1
        if(@secondes > 59)
            @secondes = 0
            @minutes += 1
            if(@minutes > 59)
                @minutes = 0
                @heures += 1
            end
        end
    end

    ##
    #   Permet de soustraire des secondes au chronomètre
    #   @param nbSecondes Le nombre de secondes à soustraire
    def soustraireSecondes(nbSecondes)
        if(@minutes > 0 || @heures > 0 || @secondes > 0)
            @secondes -= 1
            if(@secondes < 0)
                @secondes = 59
                @minutes -= 1
                if(@minutes < 0)
                    @heures -= 1
                end
            end
        else
            @lance = false
        end
    end

    ##
    #   Permet d'afficher le chronomètre 
    def gestionAffichage()
        if(@secondes < 10)
            textSecondes = "0" + @secondes.to_s
        else
            textSecondes = @secondes.to_s
        end    
        if(@minutes < 10)
            textMinutes  = "0" + @minutes.to_s
        else
            textMinutes = @secondes.to_s
        end
        if(@heures < 10)
            textHeures  = "0" + @heures.to_s
        else
            textHeures  = @heures.to_s
        end
        @lblTemps.set_markup("<b>" + textHeures + " : " + textMinutes + " : " + textSecondes + "</b>")
    end

    private_class_method :new
end