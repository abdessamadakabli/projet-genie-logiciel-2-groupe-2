require 'time'
require 'gtk3'
require 'json'
require 'fileutils'

require "../grille/Grille.rb"
require "../grille/GrilleJson.rb"

##
#   Cette classe permet de créer le profil du joueur et l'ensemble des informations 
#   qui lui sont relatives
class Profil < Page
    attr_accessor :nom, :nbTropheesCompetitif, :nbPartiesCompetitif, :nbTropheesPerfection, :nbPartiesPerfection, :grille, :nomsSauvegardesGrilles
    ##
    #   Permet d'établir le chemins vers les profils stockés en json
    CHEMIN_PROFILS           = "../jsonProfils/"
    ##
    #   Permet d'établir le chemins vers les grilles des différents profils stockés en json
    CHEMIN_PROFIL_GRILLES    = "/grilles/"
    ##
    #   Permet d'établir le chemins vers les grilles initiales
    CHEMIN_GRILLES_INITIALES = "../jsonGrilles/"

    ##
    #   Constructeur permettant d'initialiser le profil d'un nouveau joueur
    def initialize
        @nom = ""
        @dateCreation  = ""
        @nbTropheesCompetitif,@nbPartiesCompetitif   = [0,0,0],[0,0,0]
        @nbTropheesPerfection,@nbPartiesPerfection   = [0,0,0],[0,0,0]
        @grilleJson = GrilleJson.creer()
        @grille = nil
        @nomsSauvegardesGrilles = Array.new
    end

    ##
    #   new étant privée, on utilise cette méthode pour créer le profil
    def Profil.creer()
        new()
    end

    ##
    #   Permet d'obtenir Le chemin vers le dossier contenant les profils
    def getCheminProfils
        return CHEMIN_PROFILS
    end
    

    ##
    #   Permet de modifier l'état des boutons permettant de charger ou supprimer
    #   @param etat Boolean permettant de rendre clickable ou non les boutons charger et supprimer
    def setChargerEtSupprimerSensibilite(etat)
        @btnChargerProfil.sensitive = etat
        @btnSupprimerProfil.sensitive = etat
    end

    ##
    #   Permet d'initialiser l'ensemble des signaux relatives à la création de grille
    def initialiserSignaux()
        @fenetreProfil.remove(@base)

        @btnCreerProfil.sensitive = false
        setChargerEtSupprimerSensibilite(false)

        @btnCreerProfil.signal_connect('clicked'){
            ajouterProfil()
            afficherProfil()
            if(@nom!="")
                @MenuPrincipale.setBtnNouvellePartieSensibilite(true)
                @MenuPrincipale.setBtnChargerPartieSensibilite(true)
                @MenuPrincipale.setTitre("Kakuro, bienvenue "+ @nom)
                cacher
                @MenuPrincipale.afficher
                @Option.updateFichierOption
            end
        }
        @bRetour.signal_connect('clicked'){
            cacher
            @MenuPrincipale.afficher
        } 
        @saisieNom.signal_connect('insert-text'){
            if @saisieNom.text.length+1 > 3 then
                @btnCreerProfil.sensitive = true
                @btnChargerProfil.sensitive = false
            else
                @btnCreerProfil.sensitive = false
            end
        }
        @saisieNom.signal_connect('delete-text'){
            if @saisieNom.text.length > 4 then
                @btnCreerProfil.sensitive = true
                @btnChargerProfil.sensitive = false
            else
                @btnCreerProfil.sensitive = false
            end
        }
        @comboBoxProfilsExistants.signal_connect('changed') {
            nomProfil = @comboBoxProfilsExistants.active_text() 
            if(nomProfil != nil)
                chargerProfil(CHEMIN_PROFILS + nomProfil + "/" + nomProfil + ".json")
                importerGrillesInitiales()
                afficherProfil() 
            else
                @lblMessageInfos.set_text("[Message] : Aucun profil n'a été sélectionné.")
            end
            @btnChargerProfil.sensitive = true
            @btnSupprimerProfil.sensitive = true
            @btnCreerProfil.sensitive = false
            @MenuPrincipale.setBtnNouvellePartieSensibilite(true)
            @MenuPrincipale.setBtnChargerPartieSensibilite(true)
            @MenuPrincipale.setTitre("Kakuro, bienvenue "+ @nom)
        }
        @btnChargerProfil.signal_connect('clicked'){
            @MenuPrincipale.setBtnNouvellePartieSensibilite(true)
            @MenuPrincipale.setBtnChargerPartieSensibilite(true)
            @MenuPrincipale.setTitre("Kakuro, bienvenue "+ @nom)
            cacher
            @MenuPrincipale.afficher
            @Option.updateFichierOption
        }
        @btnSupprimerProfil.signal_connect('clicked'){
            supprimerProfil()
            @saisieNom.text = ""
            initialiserProfil(false)
            afficherProfil()
            @Option.updateFichierOption
            @MenuPrincipale.setTitre("Kakuro")
            @MenuPrincipale.setBtnNouvellePartieSensibilite(false)
            @MenuPrincipale.setBtnChargerPartieSensibilite(false)
            setChargerEtSupprimerSensibilite(false)
        }
    end

    ##
    #   Permet d'initialiser le profil avec les informations élémentaires
    #   @param check permet de véirfier si c'est une initialisation du proil après une suppression ou non
    def initialiserProfil(check)
        if(check)
            @nom = @saisieNom.text()
        else
            @nom = ""
        end
        date = Time.new
        dateActuelle = date.day.to_s + "_" + date.month.to_s + "_" + date.year.to_s + "_" + date.hour.to_s + "_" + date.min.to_s
        @dateCreation = dateActuelle
        @nbTropheesCompetitif, @nbPartiesCompetitif  = [0,0,0],[0,0,0]
        @nbTropheesPerfection, @nbPartiesPerfection  = [0,0,0],[0,0,0]
        @nomsSauvegardesGrilles = Array.new
        @grille = nil
    end

    ##
    #   Permet à l'utilisateur d'ajouter un nouveau profil
    def ajouterProfil()
        if(@saisieNom.text() != "")
            dossierProfil = CHEMIN_PROFILS + @saisieNom.text()
            dossierProfilGrilles = dossierProfil + CHEMIN_PROFIL_GRILLES
            if (Dir.exist?(dossierProfil) and Dir.exist?(dossierProfilGrilles))
                chargerProfil(dossierProfil + "/" + @saisieNom.text() + ".json")
                importerGrillesInitiales()
                @lblMessageInfos.set_text("[Message] : Le profil #{@nom} a été chargé")
            else
                if(!Dir.exist?(dossierProfil))
                    Dir.mkdir(dossierProfil)
                    Dir.mkdir(dossierProfilGrilles)
                else
                    if(!Dir.exist?(dossierProfilGrilles))
                        Dir.mkdir(dossierProfilGrilles)
                    end
                end
                initialiserProfil(true)
                sauvegarderProfil()
                importerGrillesInitiales()
                @lblMessageInfos.set_text("[Message] : Le profil #{@nom} a été créé et sauvegardé.")
            end
        else
            @lblMessageInfos.set_text("[Message] : Veuillez saisir un nom de profil.")
        end
        getTousLesProfils()
    end


    ##
    #   Permet à l'utilisateur de supprimer un profil
    #   On supprime le nom ainsi que le fichier contenant les sauvegardes
    def supprimerProfil()
        if @comboBoxProfilsExistants.active_text != nil
            begin
                FileUtils.remove_dir(CHEMIN_PROFILS + @comboBoxProfilsExistants.active_text)
                @lblMessageInfos.set_text("[Message] : Le profil #{@comboBoxProfilsExistants.active_text} a été supprimé.")
            rescue Errno::ENOENT
                @lblMessageInfos.set_text("[Message] : Il n'y a aucun profil à supprimer.")
            end
        else
            @lblMessageInfos.set_text("[Message] : Veuillez sélectionner le profil à supprimer.")
        end
        getTousLesProfils()
        importerNomsGrilles()
    end

    ##
    #   Permet à l'utilisateur de charger un profil
    #   @param fichier Le fichier json depuis lequel on charge le profil
    def chargerProfil(fichier)
        if @comboBoxProfilsExistants.active_text != nil || @nom != "" then
            begin
                fichierProfil = File.read(fichier)
                infos = JSON.parse(fichierProfil)
                @nom = infos["profil"]["nom"]
                @dateCreation = infos["profil"]["dateInscription"]
                @nbTropheesCompetitif = infos["profil"]["Competitif"]["nbTropheesCompetitif"]
                @nbPartiesCompetitif = infos["profil"]["Competitif"]["nbPartiesCompetitif"]
                @nbTropheesPerfection = infos["profil"]["Perfection"]["nbTropheesPerfection"]
                @nbPartiesPerfection = infos["profil"]["Perfection"]["nbPartiesPerfection"]
                @nomsSauvegardesGrilles = infos["profil"]["nomsSauvegardesGrilles"]
                @lblMessageInfos.set_text("[Message] : Le profil #{@comboBoxProfilsExistants.active_text} a été chargé.")
                return true
            rescue JSON::ParserError
                @lblMessageInfos.set_text("[Message] : Le profil n'a pas pu être chargé.")
                return false
            rescue Errno::ENOENT => e
                return false
            end
        end
    end


    ##
    #   Permet à l'utilisateur de sauvegarder un profil
    def sauvegarderProfil()
        nomFichierProfil = CHEMIN_PROFILS + @nom + "/" + @nom +".json"
        fichierProfil = File.new(nomFichierProfil, "w")
        #print("NOM SAUVEGARDE : #{jsonNomsSauvegardesGrilles()}\n")
        parseJson = JSON.parse(formatterProfilJson(jsonNomDate()+jsonNomsSauvegardesGrilles()+jsonStatistiquesClassement()))
        fichierProfil.write(JSON.pretty_generate(parseJson))
        fichierProfil.close()
    end

    
    ##
    #   Permet d'afficher le profil   
     def afficherProfil()
        @lblNombreTropheesCompetitifFacile.set_text(@nbTropheesCompetitif[0].to_s)
        @lblNombreTropheesCompetitifMoyen.set_text(@nbTropheesCompetitif[1].to_s)
        @lblNombreTropheesCompetitifDifficile.set_text(@nbTropheesCompetitif[2].to_s)

        @lblNombrePartiesCompetitifFacile.set_text(@nbPartiesCompetitif[0].to_s)
        @lblNombrePartiesCompetitifMoyen.set_text(@nbPartiesCompetitif[1].to_s)
        @lblNombrePartiesCompetitifDifficile.set_text(@nbPartiesCompetitif[2].to_s)

        @lblNombreTropheesPerfectionFacile.set_text(@nbTropheesPerfection[0].to_s)
        @lblNombreTropheesPerfectionMoyen.set_text(@nbTropheesPerfection[1].to_s)
        @lblNombreTropheesPerfectionDifficile.set_text(@nbTropheesPerfection[2].to_s)

        @lblNombrePartiesPerfectionFacile.set_text(@nbPartiesPerfection[0].to_s)
        @lblNombrePartiesPerfectionMoyen.set_text(@nbPartiesPerfection[1].to_s)
        @lblNombrePartiesPerfectionDifficile.set_text(@nbPartiesPerfection[2].to_s)

        @saisieNom.text = @nom
    end



    ##
    #   Permet de récupérer l'ensemble des profils disponibles et les mettre dans la combo Box
    def getTousLesProfils()
        nomsProfils = Dir.glob("#{CHEMIN_PROFILS}*")
        @comboBoxProfilsExistants.remove_all()
        for nomProfil in nomsProfils
            @comboBoxProfilsExistants.append_text(nomProfil.rpartition('/').last)
        end
    end

    ##
    #   Permet d'importer tous les noms de sauvegardes de grilles
    def importerNomsGrilles()
        estVide = @nomsSauvegardesGrilles.empty?
        nomGrilles = Dir.entries(CHEMIN_GRILLES_INITIALES)

        if(estVide) 
            for grille in nomGrilles
                if(File.extname(grille) == ".json")
                    @nomsSauvegardesGrilles.push([grille,"",false])
                end
            end
        end
    end

    ##
    #   Permet d'obtenir l'indice de la grille à sauvegarde 
    #   dans le tableau des noms de sauvegarde
    #   @param grilleActuelle La grille sur laquelle l'utilisateur joue à un instant donné
    def getIndiceGrille(grilleActuelle)
        indice = 0
        for grille in @nomsSauvegardesGrilles
            #print("#{grille[0]} == #{grilleActuelle} = #{grille[0] == grilleActuelle}\n")
            if(grille[0] == grilleActuelle.rpartition('/').last)
                return indice
            end
            indice+=1
        end
        return -1
    end

    ##
    #   Permet d'importer l'ensemble des grilles
    def importerGrillesInitiales()
        begin
            cheminProfil = CHEMIN_PROFILS + @nom + CHEMIN_PROFIL_GRILLES
            fichierGrillesProfil = Dir.entries(cheminProfil)
            fichierGrilllesInitiales = Dir.entries(CHEMIN_GRILLES_INITIALES)
            for grille in fichierGrilllesInitiales
                if(!fichierGrillesProfil.include?(grille))
                    if(File.extname(grille) == ".json")
                        FileUtils.cp(CHEMIN_GRILLES_INITIALES+grille,cheminProfil)
                    end
                end
            end
            importerNomsGrilles()
            return true
        rescue Errno::ENOENT => e
            print("Le fichier ou le dossier n'a pas été trouvé.")
            return false
        end
    end

    ##
    #   Permet de formatter le profil en Json
    #   @param contenu Le contenu du profil en format json
    #   @return [String] Le code json correspondant 
    def formatterProfilJson(contenu)
        return "{\"profil\": { #{contenu}}}"
    end

    ##
    #   Permet de générer le code json correspondant au nom et la date d'inscription
    #   @return [String] Le code json correspondant au nom
    def jsonNomDate()   
        jsonNom = "\"nom\": \"#{@nom}\","
        jsonDate = "\"dateInscription\": \"#{@dateCreation}\","
        return jsonNom + jsonDate
    end

    ##
    #   Permet de générer le code json correspondant aux noms de sauvegarde des grilles
    #   @return [String] Le code json correspondant aux noms de sauvegarde des grilles
    def jsonNomsSauvegardesGrilles()   
        jsonNomsSauvegardes = "\"nomsSauvegardesGrilles\": #{@nomsSauvegardesGrilles},"
        return jsonNomsSauvegardes
    end

    ##
    #   Permet de géréner le code json correspondant aux statistiques des classements
    #   @return [String] Le code json correspondant aux statistiques des classements
    def jsonStatistiquesClassement() 
        jsonModeCompetitif = "\"Competitif\": {
            \"nbTropheesCompetitif\" : #{@nbTropheesCompetitif},
            \"nbPartiesCompetitif\" : #{@nbPartiesCompetitif}
        },"

        jsonModePerfection = "\"Perfection\": {
            \"nbTropheesPerfection\" : #{@nbTropheesPerfection},
            \"nbPartiesPerfection\" : #{@nbPartiesPerfection}
        }"

        return jsonModeCompetitif + jsonModePerfection
        
    end

    ##
    #   Permet de passer à une autre langue
    #   @param infos les informations relatives à la partie, le mode et la difficulté de la grille
    def swapLangue(infos)
        @lblProfil.set_text(infos["Profil"]["Profil"])
        @lblNom.set_text(infos["Profil"]["Nom"])
        @lblProfilsExistants.set_text(infos["Profil"]["Profil Existant"])
        @lblCompetitif.set_text(infos["Profil"]["Mode Competitif"])
        @lblPerfection.set_text(infos["Profil"]["Mode Perfection"])

        @lblCompetitifFacile.set_text(infos["Profil"]["Facile"])
        @lblCompetitifMoyen.set_text(infos["Profil"]["Normal"])
        @lblCompetitifDifficile.set_text(infos["Profil"]["Difficile"])

        @lblPerfectionFacile.set_text(infos["Profil"]["Facile"])
        @lblPerfectionMoyen.set_text(infos["Profil"]["Normal"])
        @lblPerfectionDifficile.set_text(infos["Profil"]["Difficile"])

        @lblNombreTrophees.set_text(infos["Profil"]["Nombre Trophees"])
        @lblNombreParties.set_text(infos["Profil"]["Nombre Partie"])

		@btnCreerProfil.set_label(infos["Profil"]["Creer profil"])
        @bRetour.set_label(infos["Profil"]["Retour"])
        @btnSupprimerProfil.set_label(infos["Profil"]["Supprimer profil"])
        @btnChargerProfil.set_label(infos["Profil"]["Charger profil"])
    end

   
    
    private_class_method :new

end